﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.txtProductVersionName = New System.Windows.Forms.TextBox()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.txtNewProductVersionName = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.txtProductID = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.cboEnv = New System.Windows.Forms.ComboBox()
        Me.lblEnv = New System.Windows.Forms.Label()
        Me.chkLimits = New System.Windows.Forms.CheckBox()
        Me.chkEndts = New System.Windows.Forms.CheckBox()
        Me.chkSubjects = New System.Windows.Forms.CheckBox()
        Me.chkCvgItems = New System.Windows.Forms.CheckBox()
        Me.txtdbuser = New System.Windows.Forms.TextBox()
        Me.txtdbpassword = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.chkProdVer = New System.Windows.Forms.CheckBox()
        Me.chkRtnValues = New System.Windows.Forms.CheckBox()
        Me.SuspendLayout()
        '
        'txtProductVersionName
        '
        Me.txtProductVersionName.Location = New System.Drawing.Point(16, 322)
        Me.txtProductVersionName.Name = "txtProductVersionName"
        Me.txtProductVersionName.Size = New System.Drawing.Size(164, 20)
        Me.txtProductVersionName.TabIndex = 99
        Me.txtProductVersionName.Visible = False
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(204, 205)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(148, 23)
        Me.Button1.TabIndex = 10
        Me.Button1.Text = "Get Config for Product ID"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'txtNewProductVersionName
        '
        Me.txtNewProductVersionName.Location = New System.Drawing.Point(209, 322)
        Me.txtNewProductVersionName.Name = "txtNewProductVersionName"
        Me.txtNewProductVersionName.Size = New System.Drawing.Size(164, 20)
        Me.txtNewProductVersionName.TabIndex = 99
        Me.txtNewProductVersionName.Visible = False
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(12, 294)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(152, 13)
        Me.Label1.TabIndex = 99
        Me.Label1.Text = "Existing Product Version Name"
        Me.Label1.Visible = False
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(205, 294)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(138, 13)
        Me.Label2.TabIndex = 99
        Me.Label2.Text = "New Product Version Name"
        Me.Label2.Visible = False
        '
        'Button2
        '
        Me.Button2.Location = New System.Drawing.Point(394, 320)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(217, 23)
        Me.Button2.TabIndex = 99
        Me.Button2.Text = "Copy Product Version to New Version"
        Me.Button2.UseVisualStyleBackColor = True
        Me.Button2.Visible = False
        '
        'txtProductID
        '
        Me.txtProductID.Location = New System.Drawing.Point(204, 33)
        Me.txtProductID.Name = "txtProductID"
        Me.txtProductID.Size = New System.Drawing.Size(86, 20)
        Me.txtProductID.TabIndex = 3
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(201, 17)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(58, 13)
        Me.Label3.TabIndex = 7
        Me.Label3.Text = "Product ID"
        '
        'Button3
        '
        Me.Button3.Location = New System.Drawing.Point(370, 205)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(148, 23)
        Me.Button3.TabIndex = 11
        Me.Button3.Text = "Update Product Config"
        Me.Button3.UseVisualStyleBackColor = True
        '
        'cboEnv
        '
        Me.cboEnv.FormattingEnabled = True
        Me.cboEnv.Items.AddRange(New Object() {"PO", "PO2", "Staging", "Prod"})
        Me.cboEnv.Location = New System.Drawing.Point(25, 33)
        Me.cboEnv.Name = "cboEnv"
        Me.cboEnv.Size = New System.Drawing.Size(121, 21)
        Me.cboEnv.TabIndex = 0
        '
        'lblEnv
        '
        Me.lblEnv.AutoSize = True
        Me.lblEnv.Location = New System.Drawing.Point(22, 17)
        Me.lblEnv.Name = "lblEnv"
        Me.lblEnv.Size = New System.Drawing.Size(66, 13)
        Me.lblEnv.TabIndex = 10
        Me.lblEnv.Text = "Environment"
        '
        'chkLimits
        '
        Me.chkLimits.AutoSize = True
        Me.chkLimits.Location = New System.Drawing.Point(204, 66)
        Me.chkLimits.Name = "chkLimits"
        Me.chkLimits.Size = New System.Drawing.Size(72, 17)
        Me.chkLimits.TabIndex = 4
        Me.chkLimits.Text = "Get Limits"
        Me.chkLimits.UseVisualStyleBackColor = True
        '
        'chkEndts
        '
        Me.chkEndts.AutoSize = True
        Me.chkEndts.Location = New System.Drawing.Point(204, 89)
        Me.chkEndts.Name = "chkEndts"
        Me.chkEndts.Size = New System.Drawing.Size(73, 17)
        Me.chkEndts.TabIndex = 5
        Me.chkEndts.Text = "Get Endts"
        Me.chkEndts.UseVisualStyleBackColor = True
        '
        'chkSubjects
        '
        Me.chkSubjects.AutoSize = True
        Me.chkSubjects.Location = New System.Drawing.Point(204, 112)
        Me.chkSubjects.Name = "chkSubjects"
        Me.chkSubjects.Size = New System.Drawing.Size(108, 17)
        Me.chkSubjects.TabIndex = 6
        Me.chkSubjects.Text = "Get Subjectivities"
        Me.chkSubjects.UseVisualStyleBackColor = True
        '
        'chkCvgItems
        '
        Me.chkCvgItems.AutoSize = True
        Me.chkCvgItems.Checked = True
        Me.chkCvgItems.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkCvgItems.Enabled = False
        Me.chkCvgItems.Location = New System.Drawing.Point(204, 135)
        Me.chkCvgItems.Name = "chkCvgItems"
        Me.chkCvgItems.Size = New System.Drawing.Size(195, 17)
        Me.chkCvgItems.TabIndex = 7
        Me.chkCvgItems.Text = "Get Coverage Items and Retentions"
        Me.chkCvgItems.UseVisualStyleBackColor = True
        '
        'txtdbuser
        '
        Me.txtdbuser.Location = New System.Drawing.Point(25, 90)
        Me.txtdbuser.Name = "txtdbuser"
        Me.txtdbuser.Size = New System.Drawing.Size(100, 20)
        Me.txtdbuser.TabIndex = 1
        '
        'txtdbpassword
        '
        Me.txtdbpassword.Location = New System.Drawing.Point(25, 140)
        Me.txtdbpassword.Name = "txtdbpassword"
        Me.txtdbpassword.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.txtdbpassword.Size = New System.Drawing.Size(100, 20)
        Me.txtdbpassword.TabIndex = 2
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(22, 74)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(47, 13)
        Me.Label4.TabIndex = 17
        Me.Label4.Text = "DB User"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(22, 124)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(53, 13)
        Me.Label5.TabIndex = 18
        Me.Label5.Text = "Password"
        '
        'chkProdVer
        '
        Me.chkProdVer.AutoSize = True
        Me.chkProdVer.Checked = True
        Me.chkProdVer.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkProdVer.Location = New System.Drawing.Point(204, 158)
        Me.chkProdVer.Name = "chkProdVer"
        Me.chkProdVer.Size = New System.Drawing.Size(177, 17)
        Me.chkProdVer.TabIndex = 8
        Me.chkProdVer.Text = "Get Product Version and Criteria"
        Me.chkProdVer.UseVisualStyleBackColor = True
        '
        'chkRtnValues
        '
        Me.chkRtnValues.AutoSize = True
        Me.chkRtnValues.Location = New System.Drawing.Point(204, 182)
        Me.chkRtnValues.Name = "chkRtnValues"
        Me.chkRtnValues.Size = New System.Drawing.Size(127, 17)
        Me.chkRtnValues.TabIndex = 9
        Me.chkRtnValues.Text = "Get Retention Values"
        Me.chkRtnValues.UseVisualStyleBackColor = True
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(539, 251)
        Me.Controls.Add(Me.chkRtnValues)
        Me.Controls.Add(Me.chkProdVer)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.txtdbpassword)
        Me.Controls.Add(Me.txtdbuser)
        Me.Controls.Add(Me.chkCvgItems)
        Me.Controls.Add(Me.chkSubjects)
        Me.Controls.Add(Me.chkEndts)
        Me.Controls.Add(Me.chkLimits)
        Me.Controls.Add(Me.lblEnv)
        Me.Controls.Add(Me.cboEnv)
        Me.Controls.Add(Me.Button3)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.txtProductID)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.txtNewProductVersionName)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.txtProductVersionName)
        Me.Name = "Form1"
        Me.Text = "Product Config Admin"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents txtProductVersionName As TextBox
    Friend WithEvents Button1 As Button
    Friend WithEvents txtNewProductVersionName As TextBox
    Friend WithEvents Label1 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents Button2 As Button
    Friend WithEvents txtProductID As TextBox
    Friend WithEvents Label3 As Label
    Friend WithEvents Button3 As Button
    Friend WithEvents cboEnv As ComboBox
    Friend WithEvents lblEnv As Label
    Friend WithEvents chkLimits As CheckBox
    Friend WithEvents chkEndts As CheckBox
    Friend WithEvents chkSubjects As CheckBox
    Friend WithEvents chkCvgItems As CheckBox
    Friend WithEvents txtdbuser As TextBox
    Friend WithEvents txtdbpassword As TextBox
    Friend WithEvents Label4 As Label
    Friend WithEvents Label5 As Label
    Friend WithEvents chkProdVer As CheckBox
    Friend WithEvents chkRtnValues As CheckBox
End Class
