﻿Imports MySql.Data.MySqlClient
Imports System.IO
Imports System.Net
Imports System.Xml
Imports Microsoft.Office.Interop

Public Class Form1
    Dim date_time_string As String = Now.ToLocalTime.ToString("yyyyMMdd_hhmmss")

    Dim dataDirectory As String = String.Format("{0}\Data\", Environment.CurrentDirectory) 'Directory used for the Excel spreadsheet containing the configuration



    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        If txtdbuser.Text = "" Or txtdbpassword.Text = "" Then 'Rather than store credentials, the user must provide database credentials to use the tool
            MsgBox("Please provide a Snap database username and password for the selected db.")
        Else


            Dim connectionstring As String
            Dim connectionstring_prod = "Server=db.snap.nasinsurance.com;Database=auth;Uid=" & txtdbuser.Text & ";Pwd=" & txtdbpassword.Text & ";"
            Dim connectionstring_stg = "Server=db.staging.nasinsurance.com;Database=staging_auth;Uid=" & txtdbuser.Text & ";Pwd=" & txtdbpassword.Text & ";"
            Dim connectionstring_po1 = "Server=db.po.nasinsurance.com;Database=auth;Uid=" & txtdbuser.Text & ";Pwd=" & txtdbpassword.Text & ";SslMode=none;" 'Added SSL Mode = None because of errors on PO env.  May need this for other environments
            Dim connectionstring_po2 = "Server=db2.po.nasinsurance.com;Database=auth;Uid=" & txtdbuser.Text & ";Pwd=" & txtdbpassword.Text & ";"
            Select Case cboEnv.Text
                Case "PO"
                    connectionstring = connectionstring_po1
                Case "PO2"
                    connectionstring = connectionstring_po2
                Case "Staging"
                    connectionstring = connectionstring_stg
                Case "Prod"
                    connectionstring = connectionstring_prod
                Case Else
                    MsgBox("Please select an environment")
                    Exit Sub
            End Select
            GetProductConfig(txtProductID.Text, connectionstring)
            MsgBox("Product configuration saved to " & dataDirectory & "config_spreadsheet" & date_time_string & ".xlsx")
        End If
    End Sub


    Private Sub GetProductConfig(ByVal productid As String, ByVal connectionstring As String)

        'This section of code controls which parts of the product configuration are queried and added to the spreadsheet
        GetProductCoverageItemsRetentionItems(productid, connectionstring)
        If chkProdVer.Checked = True Then GetProductVersion(productid, connectionstring)
        If chkLimits.Checked = True Then GetLimitGroups(productid, connectionstring)
        If chkLimits.Checked = True Then GetAggLimitGroups(productid, connectionstring)
        If chkLimits.Checked = True Then GetAggLimitGroupsValues(productid, connectionstring)
        If chkLimits.Checked = True Then GetLimitGroupsValues(productid, connectionstring)
        If chkSubjects.Checked = True Then GetSubjectivities(productid, connectionstring)
        If chkEndts.Checked = True Then GetEndorsements(productid, connectionstring)
        If chkProdVer.Checked = True Then GetProductVersionCriteria(productid, connectionstring)
        If chkRtnValues.Checked = True Then GetRetentionGroups(productid, connectionstring)
        If chkRtnValues.Checked = True Then GetRetentionItemValues(productid, connectionstring)



    End Sub


    Private Sub GetProductCoverageItemsRetentionItems(ByVal product_id_input As String, ByVal connectionstring As String)
        Debug.Print("GetProductCoverageItemsRetentionItems")
        'This gets the coverage groups, coverage items, retention items, and associated values
        Dim c_snap As New MySqlConnection(connectionstring)

        Dim da As New MySqlDataAdapter
        Dim dr As MySqlDataReader
        Dim ds1 As New DataSet

        Dim product_id, product_name, product_version_id, product_version_name, coverage_group_id, coverage_group_name,
        coverage_group_is_primary, coverage_group_is_default,
        coverage_group_item_id, coverage_group_item_number, coverage_group_item_name, coverage_group_item_is_included_by_endorsement,
        coverage_group_item_is_enhancement, limit_type,
        retention_item_sequence, retention_item_name, retention_item_type_name, retention_item_type_value_type, retention_item_id, parent_coverage_group_item As New ArrayList

        Try
            c_snap.Open()

            'The script below converts nulls into 0 to account for coverage items that don't have retention items
            'The script contains a union because it gets the values for coverage items and coverage sub items
            'Coverage Items are the specific types of coverage offered within a product offering
            'Coverage sub items are components of coverage items that may not offer the full limits of coverage offered by the coverage item as a whole
            'Retentions are similar to deductibles
            Dim sql1 = "SELECT * 
                        FROM 
                        (SELECT *
                        FROM
                            (SELECT 
                                p1.id AS p_id,
                                    p1.name AS p_name,
                                    pv.id AS pv_id,
                                    pv.name AS pv_name,
                                    cg.id AS cg_id,
                                    cg.name AS cg_name,
                                    cg.is_primary AS cg_primary,
                                    cg.is_default AS cg_default,
                                    cgi.id AS cgi_id,
                                    cgi.number AS cgi_number,
                                    cgi.name AS cgi_name,
                                    '' AS parent_name,
                                    cgi.is_included_by_endorsement AS cgi_by_endt,
                                    cgi.is_enhancement AS cgi_is_enha,
                                    lt.type AS limit_type,
                                    IFNULL(ri.id, 0) AS ri_id,
                                    IFNULL(ri.sequence, 0) AS ri_seq,
                                    IFNULL(ri.name, '') AS ri_name,
                                    IFNULL(rit.name, '') AS ri_type,
                                    IFNULL(rit.value_type, '') AS ri_value_type,
                                    CONCAT(cgi.number, '.0', ri.sequence) AS cgi_order
                            FROM
                                product p1
                            LEFT JOIN product_version pv ON p1.id = pv.product_id
                            LEFT JOIN coverage_group cg ON pv.id = cg.product_version_id
                            LEFT JOIN coverage_group_item cgi ON cg.id = cgi.coverage_group_id
                            LEFT JOIN limit_type lt ON lt.id = cgi.limit_type_id
                            LEFT JOIN retention_item ri ON cgi.id = ri.coverage_group_item_id
                            LEFT JOIN retention_item_type rit ON rit.id = ri.retention_item_type_id
                            WHERE
                                p1.id = '" & product_id_input & "'
                            UNION SELECT 
                                p1.id AS p_id,
                                    p1.name AS p_name,
                                    pv.id AS pv_id,
                                    pv.name AS pv_name,
                                    cg.id AS cg_id,
                                    cg.name AS cg_name,
                                    cg.is_primary AS cg_primary,
                                    cg.is_default AS cg_default,
                                    cgsi.id AS cgi_id,
                                    cgsi.number AS cgi_number,
                                    cgsi.name AS cgi_name,
                                    IF(cgsi.name IS NOT NULL, cgi.name, '') AS parent_cgi,
                                    cgi.is_included_by_endorsement AS cgi_by_endt,
                                    cgi.is_enhancement AS cgi_is_enha,
                                    IFNULL(lt2.type, lt.type) AS limit_type,
                                    0 AS ri_id,
                                    0 AS ri_seq,
                                    '' AS ri_name,
                                    '' AS ri_type,
                                    '' AS ri_value_type,
                                    CONCAT(cgi.number, '.', cgsi.number, ri.sequence) AS cgi_order
                            FROM
                                product p1
                            LEFT JOIN product_version pv ON p1.id = pv.product_id
                            LEFT JOIN coverage_group cg ON pv.id = cg.product_version_id
                            LEFT JOIN coverage_group_item cgi ON cg.id = cgi.coverage_group_id
                            LEFT JOIN limit_type lt ON lt.id = cgi.limit_type_id
                            LEFT JOIN retention_item ri ON cgi.id = ri.coverage_group_item_id
                            LEFT JOIN retention_item_type rit ON rit.id = ri.retention_item_type_id
                            LEFT JOIN coverage_group_sub_item cgsi ON cgsi.coverage_group_item_id = cgi.id
                            LEFT JOIN limit_type lt2 ON lt2.id = cgsi.limit_type_id
                            WHERE
                               p1.id = '" & product_id_input & "'  
                               AND cgsi.id IS NOT NULL
                               ) AS subqry
                            ORDER BY CAST(cg_id AS UNSIGNED) + 0 , CAST(cgi_order AS DECIMAL (2)) + 0) AS subqry2
                            ORDER BY cg_id + 0 , cgi_order + 0;"


            Dim cmd = New MySqlCommand(sql1, c_snap)
            da = New MySqlDataAdapter(cmd)
            dr = cmd.ExecuteReader


            Do While dr.Read()

                product_id.Add(Int(dr.Item(0)))
                product_name.Add(CStr(dr.Item(1)))
                product_version_id.Add(Int(dr.Item(2)))
                product_version_name.Add(CStr(dr.Item(3)))
                coverage_group_id.Add(Int(dr.Item(4)))
                coverage_group_name.Add(CStr(dr.Item(5)))
                coverage_group_is_primary.Add(Int(dr.Item(6)))
                coverage_group_is_default.Add(Int(dr.Item(7)))
                coverage_group_item_id.Add(Int(dr.Item(8)))
                coverage_group_item_number.Add(Int(dr.Item(9)))
                coverage_group_item_name.Add(CStr(dr.Item(10)))
                parent_coverage_group_item.Add(CStr(dr.Item(11)))
                coverage_group_item_is_included_by_endorsement.Add(Int(dr.Item(12)))
                coverage_group_item_is_enhancement.Add(Int(dr.Item(13)))
                limit_type.Add(CStr(dr.Item(14)))
                retention_item_id.Add(Int(dr.Item(15)))
                retention_item_sequence.Add(Int(dr.Item(16)))
                retention_item_name.Add(CStr(dr.Item(17)))
                retention_item_type_name.Add(CStr(dr.Item(18)))
                retention_item_type_value_type.Add(CStr(dr.Item(19)))


            Loop
            dr.Close()



        Catch ex1 As Exception

        Finally
            c_snap.Close()

        End Try


        Dim excelapp As Excel.Application
        excelapp = New Excel.Application
        Dim excelworkbook As Excel.Workbook
        excelworkbook = excelapp.Workbooks.Add
        'excelworkbook.Sheets.Add()
        Dim excelsheet = excelworkbook.Sheets(excelworkbook.Sheets.Count)
        excelsheet.name = "CvgGrps & Itms"
        excelapp.Visible = True

        'Create the headings of the excel table
        'Names within asterisks are editable via this tool
        excelsheet.Range("A" & 1).Value = "Product ID"
        excelsheet.Range("B" & 1).Value = "Product Name"
        excelsheet.Range("C" & 1).Value = "Product Version ID"
        excelsheet.Range("D" & 1).Value = "Product Version Name"
        excelsheet.Range("E" & 1).Value = "Coverage Group Id"
        excelsheet.Range("F" & 1).Value = "Coverage Group Name"
        excelsheet.Range("G" & 1).Value = "Coverage Group is Primary"
        excelsheet.Range("H" & 1).Value = "Coverage Group is Default"
        excelsheet.Range("I" & 1).Value = "Coverage Group Item ID"
        excelsheet.Range("J" & 1).Value = "Coverage Group Item Number"
        excelsheet.Range("K" & 1).Value = "*Coverage Group Item Name*"
        excelsheet.Range("L" & 1).Value = "Parent Coverage Group Item Name"
        excelsheet.Range("M" & 1).Value = "*Coverage Group Item is Included by Endorsement*"
        excelsheet.Range("N" & 1).Value = "*Coverage Group Item is Enhancement*"
        excelsheet.Range("O" & 1).Value = "*Limit Type*"
        excelsheet.Range("P" & 1).Value = "Retention Item ID"
        excelsheet.Range("Q" & 1).Value = "*Retention Item Sequence*"
        excelsheet.Range("R" & 1).Value = "*Retention Item Name*"
        excelsheet.Range("S" & 1).Value = "*Retention Item Type*"
        excelsheet.Range("T" & 1).Value = "Retention Item Type Value Type"

        'Populate the Excel table data
        For a = 0 To product_id.Count - 1
            excelsheet.Range("A" & a + 2).Value = product_id(a)
            excelsheet.Range("B" & a + 2).Value = product_name(a)
            excelsheet.Range("C" & a + 2).Value = product_version_id(a)
            excelsheet.Range("D" & a + 2).Value = product_version_name(a)
            excelsheet.Range("E" & a + 2).Value = coverage_group_id(a)
            excelsheet.Range("F" & a + 2).Value = coverage_group_name(a)
            excelsheet.Range("G" & a + 2).Value = coverage_group_is_primary(a)
            excelsheet.Range("H" & a + 2).Value = coverage_group_is_default(a)
            excelsheet.Range("I" & a + 2).Value = coverage_group_item_id(a)
            excelsheet.Range("J" & a + 2).Value = coverage_group_item_number(a)
            excelsheet.Range("K" & a + 2).Value = coverage_group_item_name(a)
            excelsheet.Range("L" & a + 2).Value = parent_coverage_group_item(a)
            excelsheet.Range("M" & a + 2).Value = coverage_group_item_is_included_by_endorsement(a)
            excelsheet.Range("N" & a + 2).Value = coverage_group_item_is_enhancement(a)
            excelsheet.Range("O" & a + 2).Value = limit_type(a)

            'if there is no retention item, don't show 0 as the ID
            If retention_item_id(a) <> 0 Then excelsheet.Range("P" & a + 2).Value = retention_item_id(a)

            'if there is no retention item, don't show 0 as the sequence
            If retention_item_sequence(a) <> 0 Then excelsheet.Range("Q" & a + 2).Value = retention_item_sequence(a)
            excelsheet.Range("R" & a + 2).Value = retention_item_name(a)
            excelsheet.Range("S" & a + 2).Value = retention_item_type_name(a)
            excelsheet.Range("T" & a + 2).Value = retention_item_type_value_type(a)
        Next a

        'format the excel tables - can't get this to work
        'excelsheet.ListObjects.Add(Excel.XlListObjectSourceType.xlSrcRange, , excelsheet.Range("$A$1:$T$") & product_id.Count, Excel.XlYesNoGuess.xlYes).Name = "Table1"
        'excelsheet.ListObjects("Table1").TableStyle = "TableStyleMedium4"

        'Code to automatically add VBA Macro spreadsheet - does not work because Programmatic access to Visual Basic Project is restricted on a "not trusted" document
        'excelworkbook.Application.VBE.ActiveVBProject.VBComponents.Import(dataDirectory & "macro.bas")

        excelworkbook.SaveAs(dataDirectory & "config_spreadsheet" & date_time_string & ".xlsx", Excel.XlFileFormat.xlWorkbookDefault)
        excelworkbook.Close(Excel.XlSaveAction.xlSaveChanges)

        excelworkbook = Nothing
        excelsheet = Nothing
        excelapp.Quit()
        excelapp = Nothing

        GC.Collect()
        GC.WaitForPendingFinalizers()

    End Sub


    Private Sub GetLimitGroupsValues(ByVal product_id_input As String, ByVal connectionstring As String)
        Debug.Print("GetLimitGroupsValues")
        Dim product_id, product_name, product_version_id, product_version_name,
                coverage_group_id, coverage_group_name, cgi_id, cgi_number, cgi_name, lt_type,
                lg_id, lg_name, lgcgi_id, lgcgi_value, parent_coverage_group_item As New ArrayList




        Dim c_snap As New MySqlConnection(connectionstring)

        Dim da As New MySqlDataAdapter
        Dim dr As MySqlDataReader
        Dim ds1 As New DataSet

        Try
            c_snap.Open()

            'Gets all the limit group values and all the associated limit values for a given coverage item or coverage sub item
            'An aggregate limit group + coverage group item or sub item yields exactly one limit value
            Dim sql1 = "SELECT 
                            *
                        FROM
                            (SELECT 
                                *
                            FROM
                                (SELECT 
                                p1.id AS p_id,
                                    p1.name AS p_name,
                                    pv.id AS pv_id,
                                    pv.name AS pv_name,
                                    cg.id AS cg_id,
                                    cg.name AS cg_name,
                                    cgi.id AS cgi_id,
                                    cgi.number AS cgi_number,
                                    cgi.name AS cgi_name,
                                    '' AS parent_cgi,
                                    lt.type AS lt_type,
                                    IFNULL(lg.id, 0) AS lg_id,
                                    IFNULL(lg.name, '') AS lg_name,
                                    IFNULL(lgcgi.id,0) AS lgcgi_id,
                                    IFNULL(lgcgi.value, 0) AS lgcgi_value,
                                    CONCAT(cgi.number, '.0') AS cgi_order
                            FROM
                                product p1
                            LEFT JOIN product_version pv ON p1.id = pv.product_id
                            LEFT JOIN coverage_group cg ON pv.id = cg.product_version_id
                            LEFT JOIN coverage_group_item cgi ON cg.id = cgi.coverage_group_id
                            LEFT JOIN limit_type lt ON lt.id = cgi.limit_type_id
                            LEFT JOIN limit_group lg ON cg.id = lg.coverage_group_id
                            LEFT JOIN limit_group_coverage_group_item lgcgi ON lg.id = lgcgi.limit_group_id
                                AND cgi.id = lgcgi.coverage_group_item_id
                            WHERE
                                p1.id = '" & product_id_input & "' 
                            UNION SELECT 
                                p1.id AS p_id,
                                    p1.name AS p_name,
                                    pv.id AS pv_id,
                                    pv.name AS pv_name,
                                    cg.id AS cg_id,
                                    cg.name AS cg_name,
                                    IFNULL(cgsi.id, cgi.id) AS cgi_id,
                                    IFNULL(cgsi.number, cgi.number) AS cgi_number,
                                    IFNULL(cgsi.name, cgi.name) AS cgi_name,
                                    IF(cgsi.name IS NOT NULL, cgi.name, '') AS parent_cgi,
                                    IFNULL(lt2.type, lt.type) AS lt_type,
                                    IFNULL(lg.id, 0) AS lg_id,
                                    IFNULL(lg.name, '') AS lg_name,
                                    IFNULL(IFNULL(lgcgsi.id, lgcgi.id),0) AS lgcgi_id,
                                    IFNULL(IFNULL(lgcgsi.value, lgcgi.value),0) AS lgcgi_value,
                                    CONCAT(cgi.number, '.', cgsi.number) AS cgi_order
                            FROM
                                product p1
                            LEFT JOIN product_version pv ON p1.id = pv.product_id
                            LEFT JOIN coverage_group cg ON pv.id = cg.product_version_id
                            LEFT JOIN coverage_group_item cgi ON cg.id = cgi.coverage_group_id
                            LEFT JOIN limit_type lt ON lt.id = cgi.limit_type_id
                            LEFT JOIN limit_group lg ON cg.id = lg.coverage_group_id
                            LEFT JOIN limit_group_coverage_group_item lgcgi ON lg.id = lgcgi.limit_group_id
                                AND cgi.id = lgcgi.coverage_group_item_id
                            LEFT JOIN coverage_group_sub_item cgsi ON cgsi.coverage_group_item_id = cgi.id
                            LEFT JOIN limit_type lt2 ON lt2.id = cgsi.limit_type_id
                            LEFT JOIN limit_group_coverage_group_sub_item lgcgsi ON lg.id = lgcgsi.limit_group_id
                                AND cgsi.id = lgcgsi.coverage_group_sub_item_id
                            WHERE
                                p1.id = '" & product_id_input & "' AND cgsi.id IS NOT NULL) AS subqry1
                            ORDER BY CAST(cg_id AS UNSIGNED) + 0 , CAST(cgi_order AS DECIMAL (2)) + 0) AS subqry2
                        ORDER BY cg_id + 0 , cgi_order + 0;"


            Dim cmd = New MySqlCommand(sql1, c_snap)
            da = New MySqlDataAdapter(cmd)
            dr = cmd.ExecuteReader


            Do While dr.Read()

                product_id.Add(Int(dr.Item(0)))
                product_name.Add(CStr(dr.Item(1)))
                product_version_id.Add(Int(dr.Item(2)))
                product_version_name.Add(CStr(dr.Item(3)))
                coverage_group_id.Add(Int(dr.Item(4)))
                coverage_group_name.Add(CStr(dr.Item(5)))
                cgi_id.Add(Int(dr.Item(6)))
                cgi_number.Add(Int(dr.Item(7)))
                cgi_name.Add(CStr(dr.Item(8)))
                parent_coverage_group_item.Add(CStr(dr.Item(9)))
                lt_type.Add(CStr(dr.Item(10)))
                lg_id.Add(Int(dr.Item(11)))
                lg_name.Add(CStr(dr.Item(12)))
                lgcgi_id.Add(Int(dr.Item(13)))
                lgcgi_value.Add(CStr(dr.Item(14)))
            Loop
            dr.Close()



        Catch ex1 As Exception

            Debug.Print(ex1.Message)


        Finally
            c_snap.Close()
            c_snap = Nothing
            dr = Nothing
        End Try

        Dim excelapp As Excel.Application
        excelapp = New Excel.Application
        Dim excelworkbook As Excel.Workbook
        excelworkbook = excelapp.Workbooks.Open(dataDirectory & "config_spreadsheet" & date_time_string & ".xlsx")
        excelworkbook.Sheets.Add()
        Dim excelsheet = excelworkbook.Sheets(1)
        excelsheet.name = "Cvg Itm Lmts"
        excelapp.Visible = False

        'Create the headings of the excel table
        'Names within asterisks are editable via this tool
        excelsheet.Range("A" & 1).Value = "Product ID"
        excelsheet.Range("B" & 1).Value = "product name"
        excelsheet.Range("C" & 1).Value = "product version id"
        excelsheet.Range("D" & 1).Value = "product version"
        excelsheet.Range("E" & 1).Value = "Cvg Grp id"
        excelsheet.Range("F" & 1).Value = "Cvg Grp name"
        excelsheet.Range("G" & 1).Value = "Cvg Grp Itm ID"
        excelsheet.Range("H" & 1).Value = "Cvg Grp Itm Num"
        excelsheet.Range("I" & 1).Value = "Cvg Grp Itm Name"
        excelsheet.Range("J" & 1).Value = "Parent Cvg Grp Itm"
        excelsheet.Range("K" & 1).Value = "Limit Type"
        excelsheet.Range("L" & 1).Value = "lmt_grp_id"
        excelsheet.Range("M" & 1).Value = "lmt_grp_name"
        excelsheet.Range("N" & 1).Value = "lmt_grp_cvg_grp_itm_id"
        excelsheet.Range("O" & 1).Value = "*lmt_grp_cvg_grp_itm_value*"

        'Populate the Excel table data
        For a = 0 To product_id.Count - 1
            excelsheet.Range("A" & a + 2).Value = product_id(a)
            excelsheet.Range("B" & a + 2).Value = product_name(a)
            excelsheet.Range("C" & a + 2).Value = product_version_id(a)
            excelsheet.Range("D" & a + 2).Value = product_version_name(a)
            excelsheet.Range("E" & a + 2).Value = coverage_group_id(a)
            excelsheet.Range("F" & a + 2).Value = coverage_group_name(a)
            excelsheet.Range("G" & a + 2).Value = cgi_id(a)
            excelsheet.Range("H" & a + 2).Value = cgi_number(a)
            excelsheet.Range("I" & a + 2).Value = cgi_name(a)
            excelsheet.Range("J" & a + 2).Value = parent_coverage_group_item(a)
            excelsheet.Range("K" & a + 2).Value = lt_type(a)
            excelsheet.Range("L" & a + 2).Value = lg_id(a)
            excelsheet.Range("M" & a + 2).Value = lg_name(a)
            excelsheet.Range("N" & a + 2).Value = lgcgi_id(a)
            excelsheet.Range("O" & a + 2).Value = lgcgi_value(a)


        Next a


        excelworkbook.Save()
        excelworkbook.Close()


        excelworkbook = Nothing
        excelsheet = Nothing
        excelapp.Quit()
        excelapp = Nothing

        GC.Collect()
        GC.WaitForPendingFinalizers()
    End Sub

    Private Sub GetLimitGroups(ByVal product_id_input As String, ByVal connectionstring As String)
        Debug.Print("GetLimitGroups")
        Dim product_id, product_name, product_version_id, product_version_name,
                coverage_group_id, coverage_group_name,
                lg_id, lg_name, lg_value As New ArrayList




        Dim c_snap As New MySqlConnection(connectionstring)

        Dim da As New MySqlDataAdapter
        Dim dr As MySqlDataReader
        Dim ds1 As New DataSet

        Try
            c_snap.Open()

            'Gets just the list of limit groups for each coverage group (i.e. does not get the individual limits for coverage items)
            Dim sql1 = "SELECT 
                            p1.id,
                            p1.name,
                            pv.id,
                            pv.name,
                            cg.id,
                            cg.name,
                            lg.id,
                            lg.name,
                            lg.value
                        FROM
                            product p1
                                LEFT JOIN
                            product_version pv ON p1.id = pv.product_id
                                LEFT JOIN
                            coverage_group cg ON pv.id = cg.product_version_id
                                LEFT JOIN
                            limit_group lg ON cg.id = lg.coverage_group_id
                        WHERE
                            p1.id = '" & product_id_input & "'
                        ORDER BY pv.product_id , pv.name , cg.id, lg.value;"


            Dim cmd = New MySqlCommand(sql1, c_snap)
            da = New MySqlDataAdapter(cmd)
            dr = cmd.ExecuteReader


            Do While dr.Read()

                product_id.Add(Int(dr.Item(0)))
                product_name.Add(CStr(dr.Item(1)))
                product_version_id.Add(Int(dr.Item(2)))
                product_version_name.Add(CStr(dr.Item(3)))
                coverage_group_id.Add(Int(dr.Item(4)))
                coverage_group_name.Add(CStr(dr.Item(5)))
                lg_id.Add(Int(dr.Item(6)))
                lg_name.Add(CStr(dr.Item(7)))
                lg_value.Add(CInt(dr.Item(8)))

            Loop
            dr.Close()



        Catch ex1 As Exception

            Debug.Print(ex1.Message)


        Finally
            c_snap.Close()
            c_snap = Nothing
            dr = Nothing
        End Try

        Dim excelapp As Excel.Application
        excelapp = New Excel.Application
        Dim excelworkbook As Excel.Workbook
        excelworkbook = excelapp.Workbooks.Open(dataDirectory & "config_spreadsheet" & date_time_string & ".xlsx")
        excelworkbook.Sheets.Add()
        Dim excelsheet = excelworkbook.Sheets(1)
        excelsheet.name = "Lmt Grps"
        excelapp.Visible = False

        'Create the headings of the excel table
        'Names within asterisks are editable via this tool
        'These names could be made more reader-friendly
        excelsheet.Range("A" & 1).Value = "product_id"
        excelsheet.Range("B" & 1).Value = "product_name"
        excelsheet.Range("C" & 1).Value = "product_version_id"
        excelsheet.Range("D" & 1).Value = "product_version_name"
        excelsheet.Range("E" & 1).Value = "coverage_group_id"
        excelsheet.Range("F" & 1).Value = "coverage_group_name"
        excelsheet.Range("G" & 1).Value = "lmt_grp_id"
        excelsheet.Range("H" & 1).Value = "lmt_grp_name"
        excelsheet.Range("I" & 1).Value = "Value"


        'Populate the Excel table data
        For a = 0 To product_id.Count - 1
            excelsheet.Range("A" & a + 2).Value = product_id(a)
            excelsheet.Range("B" & a + 2).Value = product_name(a)
            excelsheet.Range("C" & a + 2).Value = product_version_id(a)
            excelsheet.Range("D" & a + 2).Value = product_version_name(a)
            excelsheet.Range("E" & a + 2).Value = coverage_group_id(a)
            excelsheet.Range("F" & a + 2).Value = coverage_group_name(a)
            excelsheet.Range("G" & a + 2).Value = lg_id(a)
            excelsheet.Range("H" & a + 2).Value = lg_name(a)
            excelsheet.Range("i" & a + 2).Value = lg_value(a)



        Next a


        excelworkbook.Save()
        excelworkbook.Close()


        excelworkbook = Nothing
        excelsheet = Nothing
        excelapp.Quit()
        excelapp = Nothing

        GC.Collect()
        GC.WaitForPendingFinalizers()
    End Sub


    Private Sub GetAggLimitGroupsValues(ByVal product_id_input As String, ByVal connectionstring As String)
        Debug.Print("GetAggLimitGroupsValues")
        Dim product_id, product_name, product_version_id, product_version_name,
                coverage_group_id, coverage_group_name, cgi_id, cgi_number, cgi_name, lt_type,
                lg_id, lg_name, lgcgi_id, lgcgi_value, parent_coverage_group_item As New ArrayList




        Dim c_snap As New MySqlConnection(connectionstring)

        Dim da As New MySqlDataAdapter
        Dim dr As MySqlDataReader
        Dim ds1 As New DataSet

        Try
            c_snap.Open()
            'Gets all the Aggregate limit group values and all the associated limit values for a given coverage 
            'item or coverage sub item
            'An aggregate limit group + coverage group item or sub item yields exactly one limit value
            Dim sql1 = "SELECT 
                            *
                        FROM
                            (SELECT 
                                *
                            FROM
                                (SELECT 
                                p1.id AS p_id,
                                    p1.name AS p_name,
                                    pv.id AS pv_id,
                                    pv.name AS pv_name,
                                    cg.id AS cg_id,
                                    cg.name AS cg_name,
                                    cgi.id AS cgi_id,
                                    cgi.number AS cgi_number,
                                    cgi.name AS cgi_name,
                                    '' AS parent_cgi,
                                    lt.type AS lt_type,
                                    IFNULL(lg.id, 0) AS lg_id,
                                    IFNULL(lg.name, '') AS lg_name,
                                    IFNULL(lgcgi.id,0) AS lgcgi_id,
                                    IFNULL(lgcgi.value,0) AS lgcgi_value,
                                    CONCAT(cgi.number, '.0') AS cgi_order
                            FROM
                                product p1
                            LEFT JOIN product_version pv ON p1.id = pv.product_id
                            LEFT JOIN coverage_group cg ON pv.id = cg.product_version_id
                            LEFT JOIN coverage_group_item cgi ON cg.id = cgi.coverage_group_id
                            LEFT JOIN limit_type lt ON lt.id = cgi.limit_type_id
                            LEFT JOIN aggregate_limit_group lg ON cg.id = lg.coverage_group_id
                            LEFT JOIN aggregate_limit_group_coverage_group_item lgcgi ON lg.id = lgcgi.aggregate_limit_group_id
                                AND cgi.id = lgcgi.coverage_group_item_id
                            WHERE
                                p1.id = '" & product_id_input & "' 
                            UNION SELECT 
                                p1.id AS p_id,
                                    p1.name AS p_name,
                                    pv.id AS pv_id,
                                    pv.name AS pv_name,
                                    cg.id AS cg_id,
                                    cg.name AS cg_name,
                                    IFNULL(cgsi.id, cgi.id) AS cgi_id,
                                    IFNULL(cgsi.number, cgi.number) AS cgi_number,
                                    IFNULL(cgsi.name, cgi.name) AS cgi_name,
                                    IF(cgsi.name IS NOT NULL, cgi.name, '') AS parent_cgi,
                                    IFNULL(lt2.type, lt.type) AS lt_type,
                                    IFNULL(lg.id, 0) AS lg_id,
                                    IFNULL(lg.name, '') AS lg_name,
                                    IFNULL(IFNULL(lgcgsi.id, lgcgi.id),0) AS lgcgi_id,
                                    IFNULL(IFNULL(lgcgsi.value, lgcgi.value),0) AS lgcgi_value,
                                    CONCAT(cgi.number, '.', cgsi.number) AS cgi_order
                            FROM
                                product p1
                            LEFT JOIN product_version pv ON p1.id = pv.product_id
                            LEFT JOIN coverage_group cg ON pv.id = cg.product_version_id
                            LEFT JOIN coverage_group_item cgi ON cg.id = cgi.coverage_group_id
                            LEFT JOIN limit_type lt ON lt.id = cgi.limit_type_id
                            LEFT JOIN aggregate_limit_group lg ON cg.id = lg.coverage_group_id
                            LEFT JOIN aggregate_limit_group_coverage_group_item lgcgi ON lg.id = lgcgi.aggregate_limit_group_id
                                AND cgi.id = lgcgi.coverage_group_item_id
                            LEFT JOIN coverage_group_sub_item cgsi ON cgsi.coverage_group_item_id = cgi.id
                            LEFT JOIN limit_type lt2 ON lt2.id = cgsi.limit_type_id
                            LEFT JOIN aggregate_limit_group_coverage_group_sub_item lgcgsi ON lg.id = lgcgsi.aggregate_limit_group_id
                                AND cgsi.id = lgcgsi.coverage_group_sub_item_id
                            WHERE
                                p1.id = '" & product_id_input & "' AND cgsi.id IS NOT NULL) AS subqry1
                            ORDER BY CAST(cg_id AS UNSIGNED) + 0 , CAST(cgi_order AS DECIMAL (2)) + 0) AS subqry2
                        ORDER BY cg_id + 0 , cgi_order + 0;"


            Dim cmd = New MySqlCommand(sql1, c_snap)
            da = New MySqlDataAdapter(cmd)
            dr = cmd.ExecuteReader


            Do While dr.Read()

                product_id.Add(Int(dr.Item(0)))
                product_name.Add(CStr(dr.Item(1)))
                product_version_id.Add(Int(dr.Item(2)))
                product_version_name.Add(CStr(dr.Item(3)))
                coverage_group_id.Add(Int(dr.Item(4)))
                coverage_group_name.Add(CStr(dr.Item(5)))
                cgi_id.Add(Int(dr.Item(6)))
                cgi_number.Add(Int(dr.Item(7)))
                cgi_name.Add(CStr(dr.Item(8)))
                parent_coverage_group_item.Add(CStr(dr.Item(9)))
                lt_type.Add(CStr(dr.Item(10)))
                lg_id.Add(Int(dr.Item(11)))
                lg_name.Add(CStr(dr.Item(12)))
                lgcgi_id.Add(Int(dr.Item(13)))
                lgcgi_value.Add(CStr(dr.Item(14)))
            Loop
            dr.Close()



        Catch ex1 As Exception
            Debug.Print(ex1.Message)



        Finally
            c_snap.Close()
            c_snap = Nothing
            dr = Nothing
        End Try

        Dim excelapp As Excel.Application
        excelapp = New Excel.Application
        Dim excelworkbook As Excel.Workbook
        excelworkbook = excelapp.Workbooks.Open(dataDirectory & "config_spreadsheet" & date_time_string & ".xlsx")
        excelworkbook.Sheets.Add()
        Dim excelsheet = excelworkbook.Sheets(1)
        excelsheet.name = "Cvg Itm Agg Lmts"
        excelapp.Visible = False

        'Create the headings of the excel table
        'Names within asterisks are editable via this tool
        excelsheet.Range("A" & 1).Value = "product_id"
        excelsheet.Range("B" & 1).Value = "product_name"
        excelsheet.Range("C" & 1).Value = "product_version_id"
        excelsheet.Range("D" & 1).Value = "product_version_name"
        excelsheet.Range("E" & 1).Value = "coverage_group_id"
        excelsheet.Range("F" & 1).Value = "coverage_group_name"
        excelsheet.Range("G" & 1).Value = "cvg_grp_itm_id"
        excelsheet.Range("H" & 1).Value = "cvg_grp_itm_number"
        excelsheet.Range("I" & 1).Value = "cvg_grp_itm_name"
        excelsheet.Range("J" & 1).Value = "Parent Coverage Group Item"
        excelsheet.Range("K" & 1).Value = "limit_type"
        excelsheet.Range("L" & 1).Value = "agg_lmt_grp_id"
        excelsheet.Range("M" & 1).Value = "agg_lmt_grp_name"
        excelsheet.Range("N" & 1).Value = "agg_lmt_grp_cvg_grp_itm_id"
        excelsheet.Range("O" & 1).Value = "*agg_lmt_grp_cvg_grp_itm_value*"

        'Populate the Excel table data
        For a = 0 To product_id.Count - 1
            excelsheet.Range("A" & a + 2).Value = product_id(a)
            excelsheet.Range("B" & a + 2).Value = product_name(a)
            excelsheet.Range("C" & a + 2).Value = product_version_id(a)
            excelsheet.Range("D" & a + 2).Value = product_version_name(a)
            excelsheet.Range("E" & a + 2).Value = coverage_group_id(a)
            excelsheet.Range("F" & a + 2).Value = coverage_group_name(a)
            excelsheet.Range("G" & a + 2).Value = cgi_id(a)
            excelsheet.Range("H" & a + 2).Value = cgi_number(a)
            excelsheet.Range("I" & a + 2).Value = cgi_name(a)
            excelsheet.Range("J" & a + 2).Value = parent_coverage_group_item(a)
            excelsheet.Range("K" & a + 2).Value = lt_type(a)
            excelsheet.Range("L" & a + 2).Value = lg_id(a)
            excelsheet.Range("M" & a + 2).Value = lg_name(a)
            excelsheet.Range("N" & a + 2).Value = lgcgi_id(a)
            excelsheet.Range("O" & a + 2).Value = lgcgi_value(a)




        Next a

        excelworkbook.Save()
        excelworkbook.Close()

        excelworkbook = Nothing
        excelsheet = Nothing
        excelapp.Quit()
        excelapp = Nothing

        GC.Collect()
        GC.WaitForPendingFinalizers()
    End Sub

    Private Sub GetAggLimitGroups(ByVal product_id_input As String, ByVal connectionstring As String)
        Dim product_id, product_name, product_version_id, product_version_name,
                coverage_group_id, coverage_group_name,
                lg_id, lg_name, lg_value As New ArrayList




        Dim c_snap As New MySqlConnection(connectionstring)

        Dim da As New MySqlDataAdapter
        Dim dr As MySqlDataReader
        Dim ds1 As New DataSet

        Try
            c_snap.Open()

            'Gets just the list of Aggregate limit groups for each coverage group 
            '(i.e. does not get the individual aggregate limits for coverage items)
            'A coverage group can have many aggregate limit groups
            Dim sql1 = "SELECT 
                            p1.id,
                            p1.name,
                            pv.id,
                            pv.name,
                            cg.id,
                            cg.name,
                            lg.id, 
                            lg.name,
                            lg.value
                        FROM
                            product p1
                                LEFT JOIN
                            product_version pv ON p1.id = pv.product_id
                                LEFT JOIN
                            coverage_group cg ON pv.id = cg.product_version_id
                                LEFT JOIN
                            aggregate_limit_group lg ON cg.id = lg.coverage_group_id
                        WHERE
                            p1.id = '" & product_id_input & "'
                        ORDER BY pv.product_id , pv.name , cg.id, lg.value;"


            Dim cmd = New MySqlCommand(sql1, c_snap)
            da = New MySqlDataAdapter(cmd)
            dr = cmd.ExecuteReader


            Do While dr.Read()

                product_id.Add(Int(dr.Item(0)))
                product_name.Add(CStr(dr.Item(1)))
                product_version_id.Add(Int(dr.Item(2)))
                product_version_name.Add(CStr(dr.Item(3)))
                coverage_group_id.Add(Int(dr.Item(4)))
                coverage_group_name.Add(CStr(dr.Item(5)))
                lg_id.Add(Int(dr.Item(6)))
                lg_name.Add(CStr(dr.Item(7)))
                lg_value.Add(CInt(dr.Item(8)))

            Loop
            dr.Close()



        Catch ex1 As Exception

            Debug.Print(ex1.Message)


        Finally
            c_snap.Close()
            c_snap = Nothing
            dr = Nothing
        End Try

        Dim excelapp As Excel.Application
        excelapp = New Excel.Application
        Dim excelworkbook As Excel.Workbook
        excelworkbook = excelapp.Workbooks.Open(dataDirectory & "config_spreadsheet" & date_time_string & ".xlsx")
        excelworkbook.Sheets.Add()
        Dim excelsheet = excelworkbook.Sheets(1)
        excelsheet.name = "Agg Lmt Grps"
        excelapp.Visible = False

        'Create the headings of the excel table
        'Names within asterisks are editable via this tool
        excelsheet.Range("A" & 1).Value = "product_id"
        excelsheet.Range("B" & 1).Value = "product_name"
        excelsheet.Range("C" & 1).Value = "product_version_id"
        excelsheet.Range("D" & 1).Value = "product_version_name"
        excelsheet.Range("E" & 1).Value = "coverage_group_id"
        excelsheet.Range("F" & 1).Value = "coverage_group_name"
        excelsheet.Range("G" & 1).Value = "lmt_grp_id"
        excelsheet.Range("H" & 1).Value = "lmt_grp_name"
        excelsheet.Range("I" & 1).Value = "Value"


        'Populate the Excel table data
        For a = 0 To product_id.Count - 1
            excelsheet.Range("A" & a + 2).Value = product_id(a)
            excelsheet.Range("B" & a + 2).Value = product_name(a)
            excelsheet.Range("C" & a + 2).Value = product_version_id(a)
            excelsheet.Range("D" & a + 2).Value = product_version_name(a)
            excelsheet.Range("E" & a + 2).Value = coverage_group_id(a)
            excelsheet.Range("F" & a + 2).Value = coverage_group_name(a)
            excelsheet.Range("G" & a + 2).Value = lg_id(a)
            excelsheet.Range("H" & a + 2).Value = lg_name(a)
            excelsheet.Range("i" & a + 2).Value = lg_value(a)



        Next a


        excelworkbook.Save()
        excelworkbook.Close()


        excelworkbook = Nothing
        excelsheet = Nothing
        excelapp.Quit()
        excelapp = Nothing

        GC.Collect()
        GC.WaitForPendingFinalizers()
    End Sub

    Private Sub GetSubjectivities(ByVal product_id_input As String, ByVal connectionstring As String)
        Dim c_snap As New MySqlConnection(connectionstring)

        Dim da As New MySqlDataAdapter
        Dim dr As MySqlDataReader
        Dim ds1 As New DataSet


        Dim product_id, product_name, product_version_id, product_version_name, s1_id, s1_sequence, s1_description, s1_is_due_before_binding,
            s1_conditions, s1_is_default, s1_is_active As New ArrayList

        Try
            c_snap.Open()

            'Get the list of subjectivities for a product
            'A product version has many subjectivities
            Dim sql1 = "SELECT 
                            p1.id,
                            p1.name,
                            pv.id,
                            pv.name,
                            s1.id,
                            s1.sequence,
                            s1.description,
                            s1.is_due_before_binding,
                            IFNULL(s1.conditions, ''),
                            s1.is_default,
                            s1.is_active
                        FROM
                            product p1
                                LEFT JOIN
                            product_version pv ON p1.id = pv.product_id
                                LEFT JOIN
                            subjectivity s1 ON s1.product_version_id = pv.id
                        WHERE
                            p1.id = '" & product_id_input & "'
                        ORDER BY pv.product_id , pv.name , s1.id , s1.sequence;"


            Dim cmd = New MySqlCommand(sql1, c_snap)
            da = New MySqlDataAdapter(cmd)
            dr = cmd.ExecuteReader


            Do While dr.Read()

                product_id.Add(Int(dr.Item(0)))
                product_name.Add(CStr(dr.Item(1)))
                product_version_id.Add(Int(dr.Item(2)))
                product_version_name.Add(CStr(dr.Item(3)))
                s1_id.Add(Int(dr.Item(4)))
                s1_sequence.Add(Int(dr.Item(5)))
                s1_description.Add(CStr(dr.Item(6)))
                s1_is_due_before_binding.Add(Int(dr.Item(7)))
                s1_conditions.Add(CStr(dr.Item(8)))
                s1_is_default.Add(Int(dr.Item(9)))
                s1_is_active.Add(Int(dr.Item(10)))


            Loop
            dr.Close()



        Catch ex1 As Exception

        Finally
            c_snap.Close()
            c_snap = Nothing
            dr = Nothing
        End Try

        Dim excelapp As Excel.Application
        excelapp = New Excel.Application
        Dim excelworkbook As Excel.Workbook
        excelworkbook = excelapp.Workbooks.Open(dataDirectory & "config_spreadsheet" & date_time_string & ".xlsx")
        excelworkbook.Sheets.Add()
        Dim excelsheet = excelworkbook.Sheets(1)

        excelsheet.name = "Subjectivities"
        excelapp.Visible = False

        'Create the headings of the excel table
        'Names within asterisks are editable via this tool
        excelsheet.Range("A" & 1).Value = "product_id"
        excelsheet.Range("B" & 1).Value = "product_name"
        excelsheet.Range("C" & 1).Value = "product_version_id"
        excelsheet.Range("D" & 1).Value = "product_version_name"
        excelsheet.Range("E" & 1).Value = "subj_id"
        excelsheet.Range("F" & 1).Value = "*subj_sequence*"
        excelsheet.Range("G" & 1).Value = "*subj_description*"
        excelsheet.Range("H" & 1).Value = "*subj_is_due_before_binding*"
        excelsheet.Range("I" & 1).Value = "*subj_conditions*"
        excelsheet.Range("J" & 1).Value = "*subj_is_default*"
        excelsheet.Range("K" & 1).Value = "*subj_is_active*"

        'Populate the Excel table data
        For a = 0 To product_id.Count - 1
            excelsheet.Range("A" & a + 2).Value = product_id(a)
            excelsheet.Range("B" & a + 2).Value = product_name(a)
            excelsheet.Range("C" & a + 2).Value = product_version_id(a)
            excelsheet.Range("D" & a + 2).Value = product_version_name(a)
            excelsheet.Range("E" & a + 2).Value = s1_id(a)
            excelsheet.Range("F" & a + 2).Value = s1_sequence(a)
            excelsheet.Range("G" & a + 2).Value = s1_description(a)
            excelsheet.Range("H" & a + 2).Value = s1_is_due_before_binding(a)
            excelsheet.Range("I" & a + 2).Value = s1_conditions(a)
            excelsheet.Range("J" & a + 2).Value = s1_is_default(a)
            excelsheet.Range("K" & a + 2).Value = s1_is_active(a)
        Next a

        excelworkbook.Save()
        excelworkbook.Close()

        excelworkbook = Nothing
        excelsheet = Nothing
        excelapp.Quit()
        excelapp = Nothing

        GC.Collect()
        GC.WaitForPendingFinalizers()

    End Sub

    Private Sub GetProductVersion(ByVal product_id_input As String, ByVal connectionstring As String)
        Dim c_snap As New MySqlConnection(connectionstring)

        Dim da As New MySqlDataAdapter
        Dim dr As MySqlDataReader
        Dim ds1 As New DataSet


        Dim product_id, product_name, product_version_id, product_version_name, form_number, form_url,
        line_code, contract_number, hr_code, default_policy_issuance_fee,
        wholesale_rate, retail_rate, grandfathered_rate, renewal_wholesale_rate, renewal_retail_rate, renewal_grandfathered_rate,
        carrier_rate, intermediate_rate,
        optional_enhancements, default_minimum_earned_premium, form_name As New ArrayList

        Try
            c_snap.Open()

            'Get the product version details for a product
            'Each product version has a single row in the database for the product version details
            'Each product version is associated with a product.  A product can have many product versions
            Dim sql1 = "SELECT 
                            p1.id,
                            p1.name,
                            pv.id,
                            pv.name,
                            f1.name, 
                            f1.number,
                            f1.url,
                            pv.line_code,
                            c1.code,
                            pv.hr_code,
                            pv.default_policy_issuance_fee,
                            pvc1.wholesale_rate,
                            pvc1.retail_rate,
                            pvc1.grandfathered_rate,
                            pvc1.renewal_wholesale_rate,
                            pvc1.renewal_retail_rate,
                            pvc1.renewal_grandfathered_rate,
                            pvc1.carrier_rate,
                            pvc1.intermediate_rate,
                            pv.optional_enhancements,
                            default_minimum_earned_premium AS 'min earned premium',
                            '' AS 'extended reporting period'
                        FROM
                            product p1
                                LEFT JOIN
                            product_version pv ON p1.id = pv.product_id
                                LEFT JOIN
                            form f1 ON f1.id = pv.policy_form_id
                                LEFT JOIN
                            product_version_commission pvc1 ON pv.product_version_commission_id = pvc1.id
                                LEFT JOIN
                            contract c1 ON pv.contract_id = c1.id
                        WHERE
                            p1.id = '" & product_id_input & "'
                        ORDER BY p1.id , pv.id;"


            Dim cmd = New MySqlCommand(sql1, c_snap)
            da = New MySqlDataAdapter(cmd)
            dr = cmd.ExecuteReader


            Do While dr.Read()

                product_id.Add(Int(dr.Item(0)))
                product_name.Add(CStr(dr.Item(1)))
                product_version_id.Add(Int(dr.Item(2)))
                product_version_name.Add(CStr(dr.Item(3)))
                form_name.Add(CStr(dr.Item(4)))
                form_number.Add(CStr(dr.Item(5)))
                form_url.Add(CStr(dr.Item(6)))
                line_code.Add(CStr(dr.Item(7)))
                contract_number.Add(CStr(dr.Item(8)))
                hr_code.Add(CStr(dr.Item(9)))
                default_policy_issuance_fee.Add(CStr(dr.Item(10)))
                wholesale_rate.Add(CDec(dr.Item(11)))
                retail_rate.Add(CDec(dr.Item(12)))
                grandfathered_rate.Add(CDec(dr.Item(13)))
                renewal_wholesale_rate.Add(CDec(dr.Item(14)))
                renewal_retail_rate.Add(CDec(dr.Item(15)))
                renewal_grandfathered_rate.Add(CDec(dr.Item(16)))
                carrier_rate.Add(CDec(dr.Item(17)))
                intermediate_rate.Add(CDec(dr.Item(18)))
                optional_enhancements.Add(CStr(dr.Item(19)))
                default_minimum_earned_premium.Add(CInt(dr.Item(20)))


            Loop
            dr.Close()



        Catch ex1 As Exception

        Finally
            c_snap.Close()
            c_snap = Nothing
            dr = Nothing
        End Try

        Dim excelapp As Excel.Application
        excelapp = New Excel.Application
        Dim excelworkbook As Excel.Workbook
        excelworkbook = excelapp.Workbooks.Open(dataDirectory & "config_spreadsheet" & date_time_string & ".xlsx")
        excelworkbook.Sheets.Add()
        Dim excelsheet = excelworkbook.Sheets(1)

        excelsheet.name = "Prd Ver"
        excelapp.Visible = False

        'Create the headings of the excel table
        'Names within asterisks are editable via this tool
        excelsheet.Range("A" & 1).Value = "product_id"
        excelsheet.Range("B" & 1).Value = "product_name"
        excelsheet.Range("C" & 1).Value = "product_version_id"
        excelsheet.Range("D" & 1).Value = "product_version_name"
        excelsheet.Range("E" & 1).Value = "form_name"
        excelsheet.Range("F" & 1).Value = "form_number"
        excelsheet.Range("G" & 1).Value = "form_url"
        excelsheet.Range("H" & 1).Value = "line_code"
        excelsheet.Range("I" & 1).Value = "contract_number"
        excelsheet.Range("J" & 1).Value = "hr_code"
        excelsheet.Range("K" & 1).Value = "default_policy_issuance_fee"
        excelsheet.Range("L" & 1).Value = "wholesale_rate"
        excelsheet.Range("M" & 1).Value = "retail_rate"
        excelsheet.Range("N" & 1).Value = "grandfathered_rate"
        excelsheet.Range("O" & 1).Value = "renewal_wholesale_rate"
        excelsheet.Range("P" & 1).Value = "renewal_retail_rate"
        excelsheet.Range("Q" & 1).Value = "renewal_grandfathered_rate"
        excelsheet.Range("R" & 1).Value = "carrier_rate"
        excelsheet.Range("S" & 1).Value = "intermediate_rate"
        excelsheet.Range("T" & 1).Value = "optional_enhancements"
        excelsheet.Range("U" & 1).Value = "default_minimum_earned_premium"


        'Populate the Excel table data
        For a = 0 To product_id.Count - 1
            excelsheet.Range("A" & a + 2).Value = product_id(a)
            excelsheet.Range("B" & a + 2).Value = product_name(a)
            excelsheet.Range("C" & a + 2).Value = product_version_id(a)
            excelsheet.Range("D" & a + 2).Value = product_version_name(a)
            excelsheet.Range("E" & a + 2).Value = form_name(a)
            excelsheet.Range("F" & a + 2).Value = form_number(a)
            excelsheet.Range("G" & a + 2).Value = form_url(a)
            excelsheet.Range("H" & a + 2).Value = line_code(a)
            excelsheet.Range("I" & a + 2).Value = contract_number(a)
            excelsheet.Range("J" & a + 2).Value = hr_code(a)
            excelsheet.Range("K" & a + 2).Value = default_policy_issuance_fee(a)
            excelsheet.Range("L" & a + 2).Value = wholesale_rate(a)
            excelsheet.Range("M" & a + 2).Value = retail_rate(a)
            excelsheet.Range("N" & a + 2).Value = grandfathered_rate(a)
            excelsheet.Range("O" & a + 2).Value = renewal_wholesale_rate(a)
            excelsheet.Range("P" & a + 2).Value = renewal_retail_rate(a)
            excelsheet.Range("Q" & a + 2).Value = renewal_grandfathered_rate(a)
            excelsheet.Range("R" & a + 2).Value = carrier_rate(a)
            excelsheet.Range("S" & a + 2).Value = intermediate_rate(a)
            excelsheet.Range("T" & a + 2).Value = optional_enhancements(a)
            excelsheet.Range("U" & a + 2).Value = default_minimum_earned_premium(a)

        Next a

        excelworkbook.Save()
        excelworkbook.Close()

        excelworkbook = Nothing
        excelsheet = Nothing
        excelapp.Quit()
        excelapp = Nothing

        GC.Collect()
        GC.WaitForPendingFinalizers()

    End Sub


    Private Sub GetProductVersionCriteria(ByVal product_id_input As String, ByVal connectionstring As String)
        Dim c_snap As New MySqlConnection(connectionstring)

        Dim da As New MySqlDataAdapter
        Dim dr As MySqlDataReader
        Dim ds1 As New DataSet


        Dim product_id, product_name, product_version_id, product_version_name, sf_name, sf_type, bc_name, nc_code, pvc_effective_date_start,
            pvc_effective_date_end As New ArrayList

        Try
            c_snap.Open()

            'Get the product version applicability rules for each product version associated with a product
            'Each product version can (and usually does) have multiple applicability rules
            Dim sql1 = "Select 
                            p1.id,
                            p1.name,
                            pv.id,
                            pv.name,
                            sf.name,
                            sf.type,
                            bc.name,
                            nc.code,
                            pvc.effective_date_start,
                            pvc.effective_date_end
                        FROM
                            product p1
                                LEFT JOIN
                            product_version pv ON p1.id = pv.product_id
                                LEFT JOIN
                            product_version_criteria pvc ON pv.id = pvc.product_version_id
                                LEFT JOIN
                            size_factor sf ON sf.id = pvc.size_factor_id
                                LEFT JOIN
                            business_class bc ON pvc.business_class_id = bc.id
                                LEFT JOIN
                            naics_code nc ON bc.naics_code_id = nc.id
                        WHERE
                            p1.id = '" & product_id_input & "'
                        ORDER BY p1.id , pv.id , sf.type , bc.name;"


            Dim cmd = New MySqlCommand(sql1, c_snap)
            da = New MySqlDataAdapter(cmd)
            dr = cmd.ExecuteReader


            Do While dr.Read()

                product_id.Add(Int(dr.Item(0)))
                product_name.Add(CStr(dr.Item(1)))
                product_version_id.Add(Int(dr.Item(2)))
                product_version_name.Add(CStr(dr.Item(3)))
                sf_name.Add(CStr(dr.Item(4)))
                sf_type.Add(CStr(dr.Item(5)))
                bc_name.Add(CStr(dr.Item(6)))
                nc_code.Add(CStr(dr.Item(7)))
                pvc_effective_date_start.Add(CDate(dr.Item(8)).ToString("yyyy-MM-dd"))
                pvc_effective_date_end.Add(CDate(dr.Item(9)).ToString("yyyy-MM-dd"))



            Loop
            dr.Close()



        Catch ex1 As Exception

        Finally
            c_snap.Close()
            c_snap = Nothing
            dr = Nothing
        End Try

        Dim excelapp As Excel.Application
        excelapp = New Excel.Application
        Dim excelworkbook As Excel.Workbook
        excelworkbook = excelapp.Workbooks.Open(dataDirectory & "config_spreadsheet" & date_time_string & ".xlsx")
        excelworkbook.Sheets.Add()
        Dim excelsheet = excelworkbook.Sheets(1)

        excelsheet.name = "Prd Ver Criteria"
        excelapp.Visible = False

        'Create the headings of the excel table
        'Names within asterisks are editable via this tool
        excelsheet.Range("A" & 1).Value = "product_id"
        excelsheet.Range("B" & 1).Value = "product_name"
        excelsheet.Range("C" & 1).Value = "product_version_id"
        excelsheet.Range("D" & 1).Value = "product_version_name"
        excelsheet.Range("E" & 1).Value = "sf_name"
        excelsheet.Range("F" & 1).Value = "sf_type"
        excelsheet.Range("G" & 1).Value = "bc_name"
        excelsheet.Range("H" & 1).Value = "nc_code"
        excelsheet.Range("I" & 1).Value = "pvc_effective_date_start"
        excelsheet.Range("J" & 1).Value = "pvc_effective_date_end"


        'Populate the Excel table data
        For a = 0 To product_id.Count - 1
            excelsheet.Range("A" & a + 2).Value = product_id(a)
            excelsheet.Range("B" & a + 2).Value = product_name(a)
            excelsheet.Range("C" & a + 2).Value = product_version_id(a)
            excelsheet.Range("D" & a + 2).Value = product_version_name(a)
            excelsheet.Range("E" & a + 2).Value = sf_name(a)
            excelsheet.Range("F" & a + 2).Value = sf_type(a)
            excelsheet.Range("G" & a + 2).Value = bc_name(a)
            excelsheet.Range("H" & a + 2).Value = nc_code(a)
            excelsheet.Range("I" & a + 2).Value = pvc_effective_date_start(a)
            excelsheet.Range("J" & a + 2).Value = pvc_effective_date_end(a)


        Next a

        excelworkbook.Save()
        excelworkbook.Close()

        excelworkbook = Nothing
        excelsheet = Nothing
        excelapp.Quit()
        excelapp = Nothing

        GC.Collect()
        GC.WaitForPendingFinalizers()

    End Sub

    Private Sub GetRetentionGroups(ByVal product_id_input As String, ByVal connectionstring As String)
        Dim c_snap As New MySqlConnection(connectionstring)

        Dim da As New MySqlDataAdapter
        Dim dr As MySqlDataReader
        Dim ds1 As New DataSet


        Dim product_version_name, cg_name, rg_name, cg_id, rg_id, pv_id As New ArrayList

        Try
            c_snap.Open()

            'Get the list of retention groups for each coverage group, for each product version associated with a product
            'A retention group is a numeric indicator that describes the deductibles that can be selected for a given coverage group
            'Each coverage group can have many retention groups
            Dim sql1 = "SELECT 
                            pv.name, cg.name, rg.id, rg.name
                        FROM
                            product p1
                                LEFT JOIN
                            product_version pv ON p1.id = pv.product_id
                                LEFT JOIN
                            coverage_group cg ON pv.id = cg.product_version_id
                                LEFT JOIN
                            retention_group rg ON cg.id = rg.coverage_group_id
                        WHERE
                            p1.id = '" & product_id_input & "'
                        ORDER BY pv.name , cg.name , rg.name;"


            Dim cmd = New MySqlCommand(sql1, c_snap)
            da = New MySqlDataAdapter(cmd)
            dr = cmd.ExecuteReader


            Do While dr.Read()


                product_version_name.Add(CStr(dr.Item(0)))
                cg_name.Add(CStr(dr.Item(1)))
                rg_id.Add(CInt(dr.Item(2)))
                rg_name.Add(CStr(dr.Item(3)))

            Loop
            dr.Close()



        Catch ex1 As Exception

        Finally
            c_snap.Close()
            c_snap = Nothing
            dr = Nothing
        End Try

        Dim excelapp As Excel.Application
        excelapp = New Excel.Application
        Dim excelworkbook As Excel.Workbook
        excelworkbook = excelapp.Workbooks.Open(dataDirectory & "config_spreadsheet" & date_time_string & ".xlsx")
        excelworkbook.Sheets.Add()
        Dim excelsheet = excelworkbook.Sheets(1)

        excelsheet.name = "Rtn Grps"
        excelapp.Visible = False


        'Create the headings of the excel table
        'Names within asterisks are editable via this tool
        excelsheet.Range("A" & 1).Value = "product_version_name"
        excelsheet.Range("B" & 1).Value = "coverage_group"
        excelsheet.Range("C" & 1).Value = "retention_group_id"
        excelsheet.Range("D" & 1).Value = "*retention_group*"


        'Populate the Excel table data
        For a = 0 To product_version_name.Count - 1

            excelsheet.Range("A" & a + 2).Value = product_version_name(a)
            excelsheet.Range("B" & a + 2).Value = cg_name(a)
            excelsheet.Range("C" & a + 2).Value = rg_id(a)
            excelsheet.Range("D" & a + 2).Value = rg_name(a)

        Next a

        excelworkbook.Save()
        excelworkbook.Close()

        excelworkbook = Nothing
        excelsheet = Nothing
        excelapp.Quit()
        excelapp = Nothing

        GC.Collect()
        GC.WaitForPendingFinalizers()

    End Sub


    Private Sub GetRetentionItemValues(ByVal product_id_input As String, ByVal connectionstring As String)
        Dim c_snap As New MySqlConnection(connectionstring)

        Dim da As New MySqlDataAdapter
        Dim dr As MySqlDataReader
        Dim ds1 As New DataSet


        Dim product_version_name, cg_name, cgi_name, ri_name, rit_name, rg_name, rgri_id, rgri_value, rit_value_type As New ArrayList

        Try
            c_snap.Open()

            'Gets the list of retention items - retention items are specific coverages for which a deductible might apply
            'Retention items are associated with coverage items (never coverage sub items)
            'There can be many zero or many retentions for a single coverage item
            Dim sql1 = "SELECT 
                            pv.name,
                            cg.name,
                            cgi.name,
                            ri.name,
                            rit.name,
                            rit.value_type,
                            rg.name,
                            IFNULL(rgri.id,0), 
                            IFNULL(rgri.value,0)
                        FROM
                            product p1
                                LEFT JOIN
                            product_version pv ON p1.id = pv.product_id
                                LEFT JOIN
                            coverage_group cg ON pv.id = cg.product_version_id
                                LEFT JOIN
                            retention_group rg ON cg.id = rg.coverage_group_id
                                LEFT JOIN
                            coverage_group_item cgi ON cgi.coverage_group_id = cg.id
                                LEFT JOIN
                            retention_item ri ON ri.coverage_group_item_id = cgi.id
                                LEFT JOIN
                            retention_group_retention_item rgri ON rgri.retention_group_id = rg.id
                                AND rgri.retention_item_id = ri.id
                                LEFT JOIN
                            retention_item_type rit ON rit.id = ri.retention_item_type_id
                        WHERE
                            p1.id = '" & product_id_input & "'
                                AND ISNULL(ri.name) = FALSE
                        ORDER BY pv.name , cg.name , cgi.name , ri.name , rit.name , rg.name;"


            Dim cmd = New MySqlCommand(sql1, c_snap)
            da = New MySqlDataAdapter(cmd)
            dr = cmd.ExecuteReader


            Do While dr.Read()


                product_version_name.Add(CStr(dr.Item(0)))
                cg_name.Add(CStr(dr.Item(1)))
                cgi_name.Add(CStr(dr.Item(2)))
                ri_name.Add(CStr(dr.Item(3)))
                rit_name.Add(CStr(dr.Item(4)))
                rit_value_type.Add(CStr(dr.Item(5)))
                rg_name.Add(CStr(dr.Item(6)))
                rgri_id.Add(CInt(dr.Item(7)))
                rgri_value.Add(CStr(dr.Item(8)))

            Loop
            dr.Close()



        Catch ex1 As Exception

        Finally
            c_snap.Close()
            c_snap = Nothing
            dr = Nothing
        End Try

        Dim excelapp As Excel.Application
        excelapp = New Excel.Application
        Dim excelworkbook As Excel.Workbook
        excelworkbook = excelapp.Workbooks.Open(dataDirectory & "config_spreadsheet" & date_time_string & ".xlsx")
        excelworkbook.Sheets.Add()
        Dim excelsheet = excelworkbook.Sheets(1)

        excelsheet.name = "Rtn Itm Vals"
        excelapp.Visible = False

        'Create the headings of the excel table
        'Names within asterisks are editable via this tool
        excelsheet.Range("A" & 1).Value = "Product Version"
        excelsheet.Range("B" & 1).Value = "Coverage Group"
        excelsheet.Range("C" & 1).Value = "Coverage Item"
        excelsheet.Range("D" & 1).Value = "Retention Item"
        excelsheet.Range("E" & 1).Value = "Retention Item Type"
        excelsheet.Range("F" & 1).Value = "Retention Item Value Type"
        excelsheet.Range("G" & 1).Value = "Retention Group"
        excelsheet.Range("H" & 1).Value = "Retention Value ID"
        excelsheet.Range("I" & 1).Value = "*Retention Value*"

        'Populate the Excel table data
        For a = 0 To product_version_name.Count - 1

            excelsheet.Range("A" & a + 2).Value = product_version_name(a)
            excelsheet.Range("B" & a + 2).Value = cg_name(a)
            excelsheet.Range("C" & a + 2).Value = cgi_name(a)
            excelsheet.Range("D" & a + 2).Value = ri_name(a)
            excelsheet.Range("E" & a + 2).Value = rit_name(a)
            excelsheet.Range("F" & a + 2).Value = rit_value_type(a)
            excelsheet.Range("G" & a + 2).Value = rg_name(a)
            excelsheet.Range("H" & a + 2).Value = rgri_id(a)
            excelsheet.Range("I" & a + 2).Value = rgri_value(a)

        Next a

        excelworkbook.Save()
        excelworkbook.Close()

        excelworkbook = Nothing
        excelsheet = Nothing
        excelapp.Quit()
        excelapp = Nothing

        GC.Collect()
        GC.WaitForPendingFinalizers()

    End Sub

    Private Sub GetEndorsements(ByVal product_id_input As String, ByVal connectionstring As String)
        Dim c_snap As New MySqlConnection(connectionstring)

        Dim da As New MySqlDataAdapter
        Dim dr As MySqlDataReader
        Dim ds1 As New DataSet


        Dim product_id, product_name, product_version_id, product_version_name, e1_id, e1_sequence, f1_name, f1_number, f1_url,
            e1_conditions, e1_is_default, e1_is_manuscript, e1_is_active, f1_id, e1_default_addl_info As New ArrayList

        Try
            c_snap.Open()

            'Gets the list of endorsements - endorsements are forms containing modifications to the terms offered by the policy document
            'Endorsments are associated with product versions, which are associated with products
            'Endorsements are also associated with forms, which are not related to products
            Dim sql1 = "SELECT 
                            p1.id,
                            p1.name,
                            pv.id,
                            pv.name,
                            e1.id,
                            e1.sequence,
                            f1.name,
                            f1.number,
                            f1.url,
                            ifnull(e1.default_additional_info, ''),
                            e1.conditions,
                            e1.is_default,
                            e1.is_manuscript,
                            e1.is_active,
                            f1.id
                        FROM
                            product p1
                                LEFT JOIN
                            product_version pv ON p1.id = pv.product_id
                                LEFT JOIN
                            endorsement e1 ON e1.product_version_id = pv.id
                                LEFT JOIN
                            form f1 ON f1.id = e1.form_id
                        WHERE
                            p1.id = '" & product_id_input & "'
                        ORDER BY pv.product_id , pv.name;"


            Dim cmd = New MySqlCommand(sql1, c_snap)
            da = New MySqlDataAdapter(cmd)
            dr = cmd.ExecuteReader


            Do While dr.Read()

                product_id.Add(Int(dr.Item(0)))
                product_name.Add(CStr(dr.Item(1)))
                product_version_id.Add(Int(dr.Item(2)))
                product_version_name.Add(CStr(dr.Item(3)))
                e1_id.Add(Int(dr.Item(4)))
                e1_sequence.Add(Int(dr.Item(5)))
                f1_name.Add(CStr(dr.Item(6)))
                f1_number.Add(CStr(dr.Item(7)))
                f1_url.Add(CStr(dr.Item(8)))
                e1_default_addl_info.Add(CStr(dr.Item(9)))
                e1_conditions.Add(CStr(dr.Item(10)))
                e1_is_default.Add(Int(dr.Item(11)))
                e1_is_manuscript.Add(Int(dr.Item(12)))
                e1_is_active.Add(Int(dr.Item(13)))
                f1_id.Add(Int(dr.Item(14)))

            Loop
            dr.Close()



        Catch ex1 As Exception

        Finally
            c_snap.Close()
            c_snap = Nothing
            dr = Nothing
        End Try

        Dim excelapp As Excel.Application
        excelapp = New Excel.Application
        Dim excelworkbook As Excel.Workbook
        excelworkbook = excelapp.Workbooks.Open(dataDirectory & "config_spreadsheet" & date_time_string & ".xlsx")
        excelworkbook.Sheets.Add()
        Dim excelsheet = excelworkbook.Sheets(1)
        excelsheet.name = "Endts"
        excelapp.Visible = False


        'Create the headings of the excel table
        'Names within asterisks are editable via this tool
        'These names could be fixed to be more reader-friendly
        excelsheet.Range("A" & 1).Value = "product_id"
        excelsheet.Range("B" & 1).Value = "product_name"
        excelsheet.Range("C" & 1).Value = "product_version_id"
        excelsheet.Range("D" & 1).Value = "product_version_name"
        excelsheet.Range("E" & 1).Value = "endt_id"
        excelsheet.Range("F" & 1).Value = "*endt_sequence*"
        excelsheet.Range("G" & 1).Value = "form_id"
        excelsheet.Range("H" & 1).Value = "*form_name*"
        excelsheet.Range("I" & 1).Value = "*form_number*"
        excelsheet.Range("J" & 1).Value = "*form_url*"
        excelsheet.Range("K" & 1).Value = "default_addl_info"
        excelsheet.Range("L" & 1).Value = "*endt_conditions*"
        excelsheet.Range("M" & 1).Value = "*endt_is_default*"
        excelsheet.Range("N" & 1).Value = "*endt_is_manuscript*"
        excelsheet.Range("O" & 1).Value = "*endt_is_active*"


        'Populate the Excel table data
        For a = 0 To product_id.Count - 1
            excelsheet.Range("A" & a + 2).Value = product_id(a)
            excelsheet.Range("B" & a + 2).Value = product_name(a)
            excelsheet.Range("C" & a + 2).Value = product_version_id(a)
            excelsheet.Range("D" & a + 2).Value = product_version_name(a)
            excelsheet.Range("E" & a + 2).Value = e1_id(a)
            excelsheet.Range("F" & a + 2).Value = e1_sequence(a)
            excelsheet.Range("G" & a + 2).Value = f1_id(a)
            excelsheet.Range("H" & a + 2).Value = f1_name(a)
            excelsheet.Range("I" & a + 2).Value = f1_number(a)
            excelsheet.Range("J" & a + 2).Value = f1_url(a)
            excelsheet.Range("K" & a + 2).Value = e1_default_addl_info(a)
            excelsheet.Range("L" & a + 2).Value = e1_conditions(a)
            excelsheet.Range("M" & a + 2).Value = e1_is_default(a)
            excelsheet.Range("N" & a + 2).Value = e1_is_manuscript(a)
            excelsheet.Range("O" & a + 2).Value = e1_is_active(a)


        Next a


        excelworkbook.Save()
        excelworkbook.Close()

        excelworkbook = Nothing
        excelsheet = Nothing
        excelapp.Quit()
        excelapp = Nothing

        GC.Collect()
        GC.WaitForPendingFinalizers()

    End Sub

    Private Sub Label3_Click(sender As Object, e As EventArgs) Handles Label3.Click

    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        If txtdbuser.Text = "" Or txtdbpassword.Text = "" Then
            MsgBox("Please provide a Snap database username and password for the selected db.")
        Else

            Dim connectionstring As String = "Server=db.po.nasinsurance.com;Database=auth;Uid=" & txtdbuser.Text & ";Pwd=" & txtdbpassword.Text & ";SslMode=none;"
            UpdateProductConfig(connectionstring)
        End If
    End Sub

    Private Sub UpdateProductConfig(ByVal connectionstring As String)


        Dim sql_stmt = "SET autocommit = 0;" 'When we run the resulting sql, we turn off auto-commit so we can roll back failed transactions
        Debug.Print(sql_stmt)
        writetolog(sql_stmt)

        sql_stmt = "start transaction;"
        Debug.Print(sql_stmt)
        writetolog(sql_stmt)

        Dim totalrows, totalcols As Integer

        Dim excelapp As Excel.Application
        excelapp = New Excel.Application
        Dim excelworkbook As Excel.Workbook
        excelapp.Visible = False
        excelworkbook = excelapp.Workbooks.Open(dataDirectory & "config_spreadsheet" & date_time_string & ".xlsx", False, True)
        Dim excelsheet As Excel.Worksheet

        If chkLimits.Checked = True Then
            excelsheet = excelworkbook.Sheets("Cvg Itm Lmts")
            totalrows = excelsheet.UsedRange.Rows.Count
            totalcols = excelsheet.UsedRange.Columns.Count

            For a = 1 To totalrows
                For b = 1 To totalcols
                    If CType(excelsheet.Cells(a, b), Excel.Range).Interior.ColorIndex = 6 Then 'Color Index 6 = Yellow highlighting
                        If b = 14 Then 'Updates the limit group coverage group item (limit value for a given coverage group item and limit group combination)
                            sql_stmt = "update limit_group_coverage_group_item set value = " & CType(excelsheet.Cells(a, 14), Excel.Range).Value & " where id = " & CType(excelsheet.Cells(a, 13), Excel.Range).Value & ";"
                            Debug.Print(sql_stmt)
                            writetolog(sql_stmt)
                        End If
                    End If

                    'insert records

                    If CType(excelsheet.Cells(a, b), Excel.Range).Interior.ColorIndex = 3 And CStr(CType(excelsheet.Cells(a, b), Excel.Range).Value) = "" Then  'inserts

                        If b = 11 Then 'if limit group id is blank and the cell is highlighted in red, insert the limit group
                            sql_stmt = "INSERT INTO `limit_group` (`coverage_group_id`, `name`, `value`) SELECT " & CType(excelsheet.Cells(a, 5), Excel.Range).Value & ",'" & CType(excelsheet.Cells(a, 12), Excel.Range).Value & "'," & convertlimitgroupnameintovalue(CType(excelsheet.Cells(a, 12), Excel.Range).Value) & " FROM `limit_group` WHERE NOT EXISTS (SELECT * FROM `limit_group` WHERE coverage_group_id=" & CType(excelsheet.Cells(a, 5), Excel.Range).Value & " AND name='" & CType(excelsheet.Cells(a, 12), Excel.Range).Value & "') LIMIT 1;"
                            Debug.Print(sql_stmt)
                            writetolog(sql_stmt)
                        End If

                        If b = 13 Then 'if limit value id is blank and the cell is highlighted in red, insert the limit value
                            Debug.Print(convertlimitgroupnameintovalue(CType(excelsheet.Cells(a, 12), Excel.Range).Value))
                            sql_stmt = "INSERT INTO limit_group_coverage_group_item (limit_group_id, coverage_group_item_id, value) values ((select id from limit_group where name = '" & CType(excelsheet.Cells(a, 12), Excel.Range).Value & "' and coverage_group_id=(select id from coverage_group where name ='" & CType(excelsheet.Cells(a, 6), Excel.Range).Value & "' and product_version_id=(select id from product_version where name ='" & CType(excelsheet.Cells(a, 4), Excel.Range).Value & "'))), (select id from coverage_group_item where name ='" & CType(excelsheet.Cells(a, 9), Excel.Range).Value & "' and coverage_group_id=(select id from coverage_group where name ='" & CType(excelsheet.Cells(a, 6), Excel.Range).Value & "' and product_version_id=(select id from product_version where name ='" & CType(excelsheet.Cells(a, 4), Excel.Range).Value & "'))), " & CType(excelsheet.Cells(a, 14), Excel.Range).Value & ");"
                            Debug.Print(sql_stmt)
                            writetolog(sql_stmt)
                        End If

                    End If

                    'To be added - insert sub item values
                Next b
            Next a



            excelsheet = excelworkbook.Sheets("Cvg Itm Agg Lmts")
            totalrows = excelsheet.UsedRange.Rows.Count
            totalcols = excelsheet.UsedRange.Columns.Count

            For a = 1 To totalrows
                For b = 1 To totalcols
                    If CType(excelsheet.Cells(a, b), Excel.Range).Interior.ColorIndex = 6 Then
                        If b = 14 Then 'Updates the aggregate limit group coverage group item (aggregate limit value for a given coverage group item and aggregate limit group combination)
                            sql_stmt = "update aggregate_limit_group_coverage_group_item set value = " & CType(excelsheet.Cells(a, 14), Excel.Range).Value & " where id = " & CType(excelsheet.Cells(a, 13), Excel.Range).Value & ";"
                            Debug.Print(sql_stmt)
                            writetolog(sql_stmt)
                        End If
                    End If

                    'insert records

                    If CType(excelsheet.Cells(a, b), Excel.Range).Interior.ColorIndex = 3 And CStr(CType(excelsheet.Cells(a, b), Excel.Range).Value) = "" Then  'inserts

                        If b = 11 Then 'if aggregate limit group id is blank and the cell is highlighted in red, insert the limit group
                            Debug.Print(convertlimitgroupnameintovalue(CType(excelsheet.Cells(a, 12), Excel.Range).Value))
                            sql_stmt = "INSERT INTO `aggregate_limit_group` (`coverage_group_id`, `name`, `value`) SELECT " & CType(excelsheet.Cells(a, 5), Excel.Range).Value & ",'" & CType(excelsheet.Cells(a, 12), Excel.Range).Value & "'," & convertlimitgroupnameintovalue(CType(excelsheet.Cells(a, 12), Excel.Range).Value) & " FROM `aggregate_limit_group` WHERE NOT EXISTS (SELECT * FROM `aggregate_limit_group` WHERE coverage_group_id=" & CType(excelsheet.Cells(a, 5), Excel.Range).Value & " AND name='" & CType(excelsheet.Cells(a, 12), Excel.Range).Value & "') LIMIT 1;"
                            Debug.Print(sql_stmt)
                            writetolog(sql_stmt)
                        End If

                        If b = 13 Then 'if aggregate limit value id is blank and the cell is highlighted in red, insert the limit value
                            sql_stmt = "INSERT INTO aggregate_limit_group_coverage_group_item (aggregate_limit_group_id, coverage_group_item_id, value) values ((select id from aggregate_limit_group where name = '" & CType(excelsheet.Cells(a, 12), Excel.Range).Value & "' and coverage_group_id=(select id from coverage_group where name ='" & CType(excelsheet.Cells(a, 6), Excel.Range).Value & "' and product_version_id=(select id from product_version where name ='" & CType(excelsheet.Cells(a, 4), Excel.Range).Value & "'))), (select id from coverage_group_item where name ='" & CType(excelsheet.Cells(a, 9), Excel.Range).Value & "' and coverage_group_id=(select id from coverage_group where name ='" & CType(excelsheet.Cells(a, 6), Excel.Range).Value & "' and product_version_id=(select id from product_version where name ='" & CType(excelsheet.Cells(a, 4), Excel.Range).Value & "'))), " & CType(excelsheet.Cells(a, 14), Excel.Range).Value & ");"
                            Debug.Print(sql_stmt)
                            writetolog(sql_stmt)
                        End If

                    End If

                Next b
            Next a

        End If

        excelsheet = excelworkbook.Sheets("Prd Ver")
        totalrows = excelsheet.UsedRange.Rows.Count
        totalcols = excelsheet.UsedRange.Columns.Count

        For a = 1 To totalrows
            For b = 1 To totalcols
                If CType(excelsheet.Cells(a, b), Excel.Range).Interior.ColorIndex = 6 Then
                    If b = 15 Then 'update optional enhancements text blurb
                        sql_stmt = "update product_version set optional_enhancements = " & CType(excelsheet.Cells(a, 15), Excel.Range).Value & " where id = " & CType(excelsheet.Cells(a, 3), Excel.Range).Value & ";"
                        Debug.Print(sql_stmt)
                        writetolog(sql_stmt)
                    End If
                End If
            Next b
        Next a

        If chkSubjects.Checked = True Then
            excelsheet = excelworkbook.Sheets("Subjectivities")
            totalrows = excelsheet.UsedRange.Rows.Count
            totalcols = excelsheet.UsedRange.Columns.Count

            For a = 1 To totalrows
                For b = 1 To totalcols
                    If CType(excelsheet.Cells(a, b), Excel.Range).Interior.ColorIndex = 6 Then 'updates
                        If b = 6 Then 'subjectivity sequence
                            sql_stmt = "update subjectivity set sequence = '" & CType(excelsheet.Cells(a, 6), Excel.Range).Value & "' where id = " & CType(excelsheet.Cells(a, 5), Excel.Range).Value & ";"
                            Debug.Print(sql_stmt)
                            writetolog(sql_stmt)
                        End If
                        If b = 7 Then 'subjectivity description
                            sql_stmt = "update subjectivity set description = '" & CType(excelsheet.Cells(a, 7), Excel.Range).Value & "' where id = " & CType(excelsheet.Cells(a, 5), Excel.Range).Value & ";"
                            Debug.Print(sql_stmt)
                            writetolog(sql_stmt)
                        End If
                        If b = 8 Then 'subjectivity is_due_before_binding
                            sql_stmt = "update subjectivity set is_due_before_binding = '" & CType(excelsheet.Cells(a, 8), Excel.Range).Value & "' where id = " & CType(excelsheet.Cells(a, 5), Excel.Range).Value & ";"
                            Debug.Print(sql_stmt)
                            writetolog(sql_stmt)
                        End If
                        If b = 9 Then 'subjectivity conditions
                            sql_stmt = "update subjectivity set conditions = '" & CType(excelsheet.Cells(a, 9), Excel.Range).Value & "' where id = " & CType(excelsheet.Cells(a, 5), Excel.Range).Value & ";"
                            Debug.Print(sql_stmt)
                            writetolog(sql_stmt)
                        End If
                        If b = 10 Then 'subjectivity is_default
                            sql_stmt = "update subjectivity set is_default = '" & CType(excelsheet.Cells(a, 10), Excel.Range).Value & "' where id = " & CType(excelsheet.Cells(a, 5), Excel.Range).Value & ";"
                            Debug.Print(sql_stmt)
                            writetolog(sql_stmt)
                        End If
                        If b = 11 Then 'is_active
                            sql_stmt = "update subjectivity set is_active = '" & CType(excelsheet.Cells(a, 11), Excel.Range).Value & "' where id = " & CType(excelsheet.Cells(a, 5), Excel.Range).Value & ";"
                            Debug.Print(sql_stmt)
                            writetolog(sql_stmt)
                        End If

                    End If

                    If CType(excelsheet.Cells(a, b), Excel.Range).Interior.ColorIndex = 3 Then  'inserts
                        If b = 5 Then 'if subjectivity id is blank, and the cell is highlighted in red, insert the subjectivity record
                            sql_stmt = "INSERT INTO `subjectivity` (`description`, `conditions`, `sequence`, `product_version_id`, `is_due_before_binding`, `is_default`, `is_active`) VALUES ('" & CType(excelsheet.Cells(a, 7), Excel.Range).Value & "', '" & CType(excelsheet.Cells(a, 9), Excel.Range).Value & "', " & CType(excelsheet.Cells(a, 6), Excel.Range).Value & ", (select id from product_version where name ='" & CType(excelsheet.Cells(a, 4), Excel.Range).Value & "'), " & CType(excelsheet.Cells(a, 8), Excel.Range).Value & ", " & CType(excelsheet.Cells(a, 10), Excel.Range).Value & ", " & CType(excelsheet.Cells(a, 11), Excel.Range).Value & ");"
                            Debug.Print(sql_stmt)
                            writetolog(sql_stmt)
                        End If

                    End If
                Next b
            Next a

        End If

        If chkEndts.Checked = True Then

            excelsheet = excelworkbook.Sheets("Endts")
            totalrows = excelsheet.UsedRange.Rows.Count
            totalcols = excelsheet.UsedRange.Columns.Count

            For a = 1 To totalrows
                For b = 1 To totalcols
                    If CType(excelsheet.Cells(a, b), Excel.Range).Interior.ColorIndex = 6 Then
                        If b = 6 Then 'endorsement sequence
                            sql_stmt = "update endorsement set sequence = '" & CType(excelsheet.Cells(a, 6), Excel.Range).Value & "' where id = " & CType(excelsheet.Cells(a, 5), Excel.Range).Value & ";"
                            Debug.Print(sql_stmt)
                            writetolog(sql_stmt)
                        End If
                        If b = 8 Then 'endorsement name
                            sql_stmt = "update form set name = '" & CType(excelsheet.Cells(a, 8), Excel.Range).Value & "' where id = " & CType(excelsheet.Cells(a, 7), Excel.Range).Value & ";"
                            Debug.Print(sql_stmt)
                            writetolog(sql_stmt)
                        End If
                        If b = 9 Then 'form number
                            sql_stmt = "update form set number = '" & CType(excelsheet.Cells(a, 9), Excel.Range).Value & "' where id = " & CType(excelsheet.Cells(a, 7), Excel.Range).Value & ";"
                            Debug.Print(sql_stmt)
                            writetolog(sql_stmt)
                        End If
                        If b = 10 Then 'form url
                            sql_stmt = "update form set url = '" & CType(excelsheet.Cells(a, 10), Excel.Range).Value & "' where id = " & CType(excelsheet.Cells(a, 7), Excel.Range).Value & ";"
                            Debug.Print(sql_stmt)
                            writetolog(sql_stmt)
                        End If
                        If b = 11 Then 'endorsement condition
                            sql_stmt = "update endorsement set conditions = '" & CType(excelsheet.Cells(a, 11), Excel.Range).Value & "' where id = " & CType(excelsheet.Cells(a, 5), Excel.Range).Value & ";"
                            Debug.Print(sql_stmt)
                            writetolog(sql_stmt)
                        End If
                        If b = 12 Then 'endorsement is_default
                            sql_stmt = "update endorsement set is_default = '" & CType(excelsheet.Cells(a, 12), Excel.Range).Value & "' where id = " & CType(excelsheet.Cells(a, 5), Excel.Range).Value & ";"
                            Debug.Print(sql_stmt)
                            writetolog(sql_stmt)
                        End If
                        If b = 13 Then 'endorsement is_manuscript
                            sql_stmt = "update endorsement set is_manuscript = '" & CType(excelsheet.Cells(a, 13), Excel.Range).Value & "' where id = " & CType(excelsheet.Cells(a, 5), Excel.Range).Value & ";"
                            Debug.Print(sql_stmt)
                            writetolog(sql_stmt)
                        End If
                        If b = 14 Then 'endorsement is_active
                            sql_stmt = "update endorsement set is_active = '" & CType(excelsheet.Cells(a, 14), Excel.Range).Value & "' where id = " & CType(excelsheet.Cells(a, 5), Excel.Range).Value & ";"
                            Debug.Print(sql_stmt)
                            writetolog(sql_stmt)
                        End If
                    End If

                    If CType(excelsheet.Cells(a, b), Excel.Range).Interior.ColorIndex = 3 And CStr(CType(excelsheet.Cells(a, b), Excel.Range).Value) = "" Then  'inserts

                        If b = 7 Then 'if form id is blank and cell is highlighted in red, insert into the form table
                            sql_stmt = "INSERT INTO `form` (`number`, `url`, `type`, `name`) VALUES ('" & CType(excelsheet.Cells(a, 9), Excel.Range).Value & "', '" & CType(excelsheet.Cells(a, 10), Excel.Range).Value & "', 'endorsement' , '" & CType(excelsheet.Cells(a, 8), Excel.Range).Value & "');"
                            Debug.Print(sql_stmt)
                            writetolog(sql_stmt)
                        End If

                        If b = 5 Then 'if the endorsement id is blank, and cell highlighted in red, insert the endorsement (getting the form id via embedded query)
                            sql_stmt = "INSERT INTO `endorsement` (`conditions`, `sequence`, `is_default`, `is_active`, `is_manuscript`, `product_version_id`, `form_id`) VALUES ('" & CType(excelsheet.Cells(a, 11), Excel.Range).Value & "', " & CType(excelsheet.Cells(a, 6), Excel.Range).Value & ", " & CType(excelsheet.Cells(a, 13), Excel.Range).Value & ", " & CType(excelsheet.Cells(a, 15), Excel.Range).Value & ", " & CType(excelsheet.Cells(a, 14), Excel.Range).Value & ", (select id from product_version where name ='" & CType(excelsheet.Cells(a, 4), Excel.Range).Value & "'), (select id from form where number ='" & CType(excelsheet.Cells(a, 9), Excel.Range).Value & "'));"
                            Debug.Print(sql_stmt)
                            writetolog(sql_stmt)
                        End If

                    End If

                Next b
            Next a

        End If

        If chkCvgItems.Checked = True Then
            excelsheet = excelworkbook.Sheets("CvgGrps & Itms")
            totalrows = excelsheet.UsedRange.Rows.Count
            totalcols = excelsheet.UsedRange.Columns.Count

            For a = 1 To totalrows
                For b = 1 To totalcols
                    If CType(excelsheet.Cells(a, b), Excel.Range).Interior.ColorIndex = 6 Then
                        If b = 11 Then 'Coverage Group Item Name
                            sql_stmt = "update coverage_group_item set name = '" & CType(excelsheet.Cells(a, 11), Excel.Range).Value & "' where id = " & CType(excelsheet.Cells(a, 9), Excel.Range).Value & ";"
                            Debug.Print(sql_stmt)
                            writetolog(sql_stmt)
                        End If
                        If b = 12 Then 'Coverage Group Item is included by endorsement flag
                            sql_stmt = "update coverage_group_item set is_included_by_endorsement = '" & CType(excelsheet.Cells(a, 12), Excel.Range).Value & "' where id = " & CType(excelsheet.Cells(a, 9), Excel.Range).Value & ";"
                            Debug.Print(sql_stmt)
                            writetolog(sql_stmt)
                        End If
                        If b = 13 Then 'Coverage Group Item is an enhancement flag
                            sql_stmt = "update coverage_group_item set is_enhancement = '" & CType(excelsheet.Cells(a, 13), Excel.Range).Value & "' where id = " & CType(excelsheet.Cells(a, 9), Excel.Range).Value & ";"
                            Debug.Print(sql_stmt)
                            writetolog(sql_stmt)
                        End If
                        If b = 14 Then 'Limit Type ID (Coverages are "limited" to a certain amount by limit type - e.g. "per claim")
                            sql_stmt = "update coverage_group_item set limit_type_id = (select id from limit_type where type = '" & CType(excelsheet.Cells(a, 14), Excel.Range).Value & "') where id = " & CType(excelsheet.Cells(a, 9), Excel.Range).Value & ";"
                            Debug.Print(sql_stmt)
                            writetolog(sql_stmt)
                        End If
                        If b = 16 Then 'Retention item sequence - indicates the order in which the retention (deductible) is listed
                            sql_stmt = "update retention_item set sequence = " & CType(excelsheet.Cells(a, 16), Excel.Range).Value & " where id = " & CType(excelsheet.Cells(a, 15), Excel.Range).Value & ";"
                            Debug.Print(sql_stmt)
                            writetolog(sql_stmt)
                        End If
                        If b = 17 Then 'Retention Item Name
                            sql_stmt = "update retention_item set name = '" & CType(excelsheet.Cells(a, 17), Excel.Range).Value & "' where id = " & CType(excelsheet.Cells(a, 15), Excel.Range).Value & ";"
                            Debug.Print(sql_stmt)
                            writetolog(sql_stmt)
                        End If
                        If b = 17 Then 'Retention Item Type
                            sql_stmt = "update retention_item set retention_item_type_id = (select id from retention_item_type where name ='" & CType(excelsheet.Cells(a, 18), Excel.Range).Value & "') where id = " & CType(excelsheet.Cells(a, 15), Excel.Range).Value & ";"
                            Debug.Print(sql_stmt)
                            writetolog(sql_stmt)
                        End If
                    End If
                Next b
            Next a
        End If


        If chkRtnValues.Checked = True Then
            excelsheet = excelworkbook.Sheets("Rtn Grps")
            totalrows = excelsheet.UsedRange.Rows.Count
            totalcols = excelsheet.UsedRange.Columns.Count

            For a = 1 To totalrows
                For b = 1 To totalcols
                    If CType(excelsheet.Cells(a, b), Excel.Range).Interior.ColorIndex = 6 Then
                        If b = 4 Then 'Retention Group name and value (e.g. Retention Group name of 1000 means $1,000 value for most retention items)
                            sql_stmt = "update retention_group set name = '" & CType(excelsheet.Cells(a, 4), Excel.Range).Value & "', value = " & CInt(CType(excelsheet.Cells(a, 4), Excel.Range).Value) & " where id = " & CType(excelsheet.Cells(a, 3), Excel.Range).Value & ";"
                            Debug.Print(sql_stmt)
                            writetolog(sql_stmt)
                        End If


                    End If

                    If CType(excelsheet.Cells(a, b), Excel.Range).Interior.ColorIndex = 3 And CStr(CType(excelsheet.Cells(a, b), Excel.Range).Value) = "" Then  'inserts

                        If b = 3 Then 'If retention group id is blank and cell is highlighted red, insert retention group name and value
                            sql_stmt = "INSERT INTO `retention_group` (`coverage_group_id`, `name`, `value`) VALUES ((select id from coverage_group where name ='" & CType(excelsheet.Cells(a, 2), Excel.Range).Value & "' and product_version_id=(select id from product_version where name ='" & CType(excelsheet.Cells(a, 1), Excel.Range).Value & "')), '" & CType(excelsheet.Cells(a, 4), Excel.Range).Value & "', " & CType(excelsheet.Cells(a, 4), Excel.Range).Value & ");"
                            Debug.Print(sql_stmt)
                            writetolog(sql_stmt)
                        End If



                    End If
                Next b
            Next a



            excelsheet = excelworkbook.Sheets("Rtn Itm Vals")
            totalrows = excelsheet.UsedRange.Rows.Count
            totalcols = excelsheet.UsedRange.Columns.Count

            For a = 1 To totalrows
                For b = 1 To totalcols
                    If CType(excelsheet.Cells(a, b), Excel.Range).Interior.ColorIndex = 6 Then
                        If b = 8 Then 'Update retention_group_retention_item value (the deductible amount for a specific retention, when a given retention group is selected)
                            sql_stmt = "update retention_group_retention_item set value = " & CType(excelsheet.Cells(a, 8), Excel.Range).Value & "' where id = " & CType(excelsheet.Cells(a, 7), Excel.Range).Value & ";"
                            Debug.Print(sql_stmt)
                            writetolog(sql_stmt)
                        End If


                    End If

                    If CType(excelsheet.Cells(a, b), Excel.Range).Interior.ColorIndex = 3 And CStr(CType(excelsheet.Cells(a, b), Excel.Range).Value) = "" Then  'inserts

                        If b = 7 Then 'Insert a retention_group_retention_item (the retention value for a given combination of retention group and retention item)
                            sql_stmt = "INSERT INTO `retention_group_retention_item` (`retention_group_id`, `retention_item_id`, `value`) VALUES ((select id from retention_group where name ='" & CType(excelsheet.Cells(a, 6), Excel.Range).Value & "' and coverage_group_id=(select id from coverage_group where name ='" & CType(excelsheet.Cells(a, 2), Excel.Range).Value & "' and product_version_id=(select id from product_version where name ='" & CType(excelsheet.Cells(a, 1), Excel.Range).Value & "'))), (select id from retention_item where name = '" & CType(excelsheet.Cells(a, 4), Excel.Range).Value & "' and retention_item_type_id = (select id from retention_item_type where name = '" & CType(excelsheet.Cells(a, 5), Excel.Range).Value & "') and coverage_group_item_id=(select id from coverage_group_item where name = '" & CType(excelsheet.Cells(a, 3), Excel.Range).Value & "' and coverage_group_id = (select id from coverage_group where name ='" & CType(excelsheet.Cells(a, 2), Excel.Range).Value & "' and product_version_id=(select id from product_version where name ='" & CType(excelsheet.Cells(a, 1), Excel.Range).Value & "')))), " & CType(excelsheet.Cells(a, 8), Excel.Range).Value & ");"
                            Debug.Print(sql_stmt)
                            writetolog(sql_stmt)
                        End If



                    End If
                Next b
            Next a
        End If



        sql_stmt = "commit;"
        Debug.Print(sql_stmt)
        writetolog(sql_stmt)

        excelworkbook.Close()
        excelworkbook = Nothing
        excelsheet = Nothing
        excelapp.Quit()
        excelapp = Nothing

        GC.Collect()
        GC.WaitForPendingFinalizers()
        Dim filedate = CDate(Now.ToLocalTime)
        Dim nowdate1 = filedate.ToString("yyyyMMdd")
        MsgBox("SQL script created: " & dataDirectory & "temp_sql" & nowdate1 & ".sql")

    End Sub

    Private Sub writetolog(ByVal logtext As String)
        Dim filedate = CDate(Now.ToLocalTime)
        Dim nowdate1 = filedate.ToString("yyyyMMdd")
        Using writer1 As New StreamWriter(dataDirectory & "temp_sql" & nowdate1 & ".sql", vbTrue)
            writer1.WriteLine(logtext)
        End Using
    End Sub


    Private Sub run_sql_from_log(ByVal connectionstring As String)
        Dim requestfile = ""
        Dim c_snap As New MySqlConnection(connectionstring)
        Try
            c_snap.Open()



            Dim filedate = CDate(Now.ToLocalTime)
            Dim nowdate1 = filedate.ToString("yyyyMMdd")
            Using reader As StreamReader = New StreamReader(dataDirectory & "temp_sql" & nowdate1 & ".sql")
                Do While reader.EndOfStream = False

                    'turned off live updates for now.  instead, the tool simply creates the script
                    'runsql(reader.ReadLine, c_snap)
                Loop
            End Using

        Catch ex As Exception

        Finally
            c_snap.Close()
            c_snap = Nothing
            Dim filedate = CDate(Now.ToLocalTime)
            Dim nowdate1 = filedate.ToString("yyyyMMdd")
            MsgBox("SQL script created: " & dataDirectory & "temp_sql" & nowdate1 & ".sql")

        End Try

    End Sub

    Private Sub runsql(ByVal sql As String, ByVal snap_db As MySqlConnection)

        Try
            Dim cmd = New MySqlCommand(sql, snap_db)

            ' cmd.ExecuteNonQuery()
            'the above statement is disabled so we don't run any updates in an environment automatically
        Catch ex As Exception
            MsgBox(ex.Message)

        Finally

        End Try


    End Sub

    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Dim filedate = CDate(Now.ToLocalTime)
        Dim nowdate1 = filedate.ToString("yyyyMMdd")
        IO.File.Delete(dataDirectory & "temp_sql" & nowdate1 & ".sql")

    End Sub

    Private Sub lblEnv_Click(sender As Object, e As EventArgs) Handles lblEnv.Click

    End Sub

    Private Sub cboEnv_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboEnv.SelectedIndexChanged

    End Sub

    Private Function convertlimitgroupnameintovalue(ByVal limitgroupname As String)
        'In the Snap DB, we save limit group values, which are used to do UI validation on the limit group, relative to the aggregate limit group (the former cannot exceed the latter)
        'To make it easy to enter the value, we automatically covert the name (e.g. 1MM) to a value (e.g. 1000000)
        Dim limitgroupvalue As Integer = 0
        Dim instr_mm, instr_k As Integer
        instr_mm = Strings.InStr(limitgroupname, "MM")
        instr_k = Strings.InStr(limitgroupname, "k")
        If instr_mm > 0 Then
            limitgroupvalue = CInt(CDec(Strings.Left(limitgroupname, instr_mm - 1)) * 1000000)
        ElseIf instr_k > 0 Then
            limitgroupvalue = CInt(CDec(Strings.Left(limitgroupname, instr_k - 1)) * 1000)
        Else
            limitgroupvalue = 0
        End If
        Return limitgroupvalue
    End Function
End Class
