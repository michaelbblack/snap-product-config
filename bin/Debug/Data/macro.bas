Sub generate_sql()
'
' loop through the entire list of products
' if the product has a product version then use the product version listed
'   if there is a coverage group, insert that coverage group
'      if there is no coverage group, insert a dummy coverage group
'         to do: insert coverage items, sub-items

' if there is no product version, insert a dummy product version
'   insert dummy coverage group named after the product
'
Dim totalrows As Integer
Dim product_name, product_version, coverage_group As String
Dim product_version_sql, coverage_group_sql As String
Dim product_version_criteria_sql As String
Dim limit_group_sql, aggregate_limit_group_sql, coverage_item_limits_sql, coverage_item_aggregate_limits_sql, _
retention_groups_sql, retention_item_values_sql As String
Dim retention_items_sql As String
Dim endt_forms_sql As String
Dim endts_sql As String
Dim subj_sql As String
Dim mm_template_sql, mm_sql As String

'all the sql statements that we need to use
'product_version_sql = "INSERT INTO product_version (`name`, `effective_start_at`, `effective_end_at`, `business_class_id`, `size_factor_id`, `product_id`) values ('PRODUCTVERSIONNAME', '2018-01-01', '2020-01-01',1,(select id from size_factor where name ='SIZEFACTORNAME' and product_id = (select id from product where name ='PRODUCTNAME')),  (select id from product where name ='PRODUCTNAME'));"

product_version_sql = "INSERT INTO product_version (`name`, `product_id`) VALUES ('PRODUCTVERSIONNAME', (select id from product where name ='PRODUCTNAME'));"

naics_code_sql = "INSERT IGNORE INTO `naics_code` (`code`, `name`) VALUES ('NAICSCODE', 'NAICSCODE');"

business_class_sql = "INSERT INTO `business_class` (`name`, `naics_code`) SELECT 'BUSINESSCLASSNAME', 'NAICSCODE' FROM `business_class` WHERE NOT EXISTS (SELECT name, naics_code_id FROM `business_class` WHERE `name`='BUSINESSCLASSNAME') LIMIT 1;"

product_business_class_sql = "INSERT INTO `product_business_class` (`product_id`, `business_class_id`) VALUES ((select id from product where name ='PRODUCTNAME'), (select id from business_class where name ='BUSINESSCLASS'));"

product_version_criteria_sql = "INSERT INTO  product_version_criteria (`size_factor_id`, `effective_date_start`, `effective_date_end`, `business_class_id`, `product_version_id`) values ((select id from size_factor where name ='SIZEFACTORNAME' and product_id = (select id from product where name ='PRODUCTNAME')), '2018-01-01', '2020-01-01',1, (select id from product_version where name ='PRODUCTVERSIONNAME'));"

product_version_commission_sql = "INSERT INTO `product_version_commission` (`wholesale_rate`, `retail_rate`, `grandfathered_rate`, `renewal_wholesale_rate`, `renewal_retail_rate`, `renewal_grandfathered_rate`, `carrier_rate`, `intermediate_rate`) VALUES ('COMM1', 'COMM2', 'COMM3', 'COMM4', 'COMM5', 'COMM6', 'COMM7', 'COMM8');"

clear_duplicate_business_class_sql = "delete n2 FROM business_class n1, business_class n2 WHERE n1.name = n2.name AND n1.id < n2.id AND n1.id <> n2.id;"

clear_duplicate_naics_code_sql = "delete n2 FROM naics_code n1, naics_code n2 WHERE n1.code = n2.code AND n1.id < n2.id AND n1.id <> n2.id;"

clear_duplicate_product_business_class_sql = "delete n2 FROM product_business_class n1, product_business_class n2 WHERE n1.business_class_id = n2.business_class_id and n1.product_id = n2.product_id AND n1.id < n2.id AND n1.id <> n2.id;"

limit_group_sql = "INSERT INTO `limit_group` (`coverage_group_id`, `name`,  `value`)" & _
 " VALUES ((select id from coverage_group where name ='COVERAGEGROUPNAME' and product_version_id=(select id from product_version where name ='PRODUCTVERSIONNAME')), 'LIMITGROUPNAME', LIMITGROUPVALUE);"

aggregate_limit_group_sql = "INSERT INTO `aggregate_limit_group` (`coverage_group_id`, `name`,  `value`)" & _
" VALUES ((select id from coverage_group where name ='COVERAGEGROUPNAME' and product_version_id=(select id from product_version where name ='PRODUCTVERSIONNAME')), 'AGGLIMITGROUPNAME',  AGGLIMITGROUPVALUE);"

coverage_item_limits_sql = "INSERT INTO limit_group_coverage_group_item (limit_group_id, coverage_group_item_id, value) VALUES " & _
"LIMITGROUPID, (select id from coverage_group_item where name ='COVERAGEITEMNAME' and coverage_group_id =(select id from coverage_group where name ='COVERAGEGROUPNAME' and product_version_id=(select id from product_version where name ='PRODUCTVERSIONNAME'))), 'LIMITVALUE';"

coverage_item_aggregate_limits_sql = "INSERT INTO aggregate_limit_group_coverage_group_item (aggregate_limit_group_id, coverage_group_item_id, value) VALUES " & _
"AGGLIMITGROUPID, (select id from coverage_group_item where name ='COVERAGEITEMNAME' and coverage_group_id =(select id from coverage_group where name ='COVERAGEGROUPNAME' and product_version_id=(select id from product_version where name ='PRODUCTVERSIONNAME'))), 'AGGLIMITVALUE';"

coverage_sub_item_limits_sql = "INSERT INTO limit_group_coverage_group_sub_item (limit_group_id, coverage_group_sub_item_id, value) VALUES " & _
" LIMITGROUPID, (select id from coverage_group_sub_item where name ='COVERAGESUBITEMNAME' and coverage_group_item_id in (select id from coverage_group_item where name ='COVERAGEITEMNAME' and coverage_group_id =(select id from coverage_group where name ='COVERAGEGROUPNAME' and product_version_id=(select id from product_version where name ='PRODUCTVERSIONNAME')))), LIMITVALUE;"

coverage_sub_item_aggregate_limits_sql = "INSERT INTO aggregate_limit_group_coverage_group_sub_item (aggregate_limit_group_id, coverage_group_sub_item_id, value) VALUES " & _
" AGGLIMITGROUPID, (select id from coverage_group_sub_item where name ='COVERAGESUBITEMNAME' and coverage_group_item_id in (select id from coverage_group_item where name ='COVERAGEITEMNAME' and coverage_group_id =(select id from coverage_group where name ='COVERAGEGROUPNAME' and product_version_id=(select id from product_version where name ='PRODUCTVERSIONNAME')))), AGGLIMITVALUE;"

retention_groups_sql = "INSERT INTO `retention_group` (`coverage_group_id`, `name`, `value`) VALUES ((select id from coverage_group where name ='COVERAGEGROUPNAME' and product_version_id=(select id from product_version where name ='PRODUCTVERSIONNAME')), 'RETENTIONGROUPNAME', 'RETENTIONGROUPVALUE');"

retention_items_sql = "INSERT INTO `retention_item` (`name`, `sequence`, `coverage_group_item_id`,`retention_item_type_id`) VALUES ('RETENTIONITEMNAME', RETENTIONITEMSEQUENCE, (select id from coverage_group_item where name ='COVERAGEITEMNAME' and coverage_group_id=(select id from coverage_group where name ='COVERAGEGROUPNAME' and product_version_id=(select id from product_version where name ='PRODUCTVERSIONNAME'))) , (select id from retention_item_type where name = 'RETENTIONITEMTYPE' and value_type = 'RETENTIONVALUETYPE'));"

'retention_item_values_sql = "INSERT INTO `retention_group_retention_item` (`retention_group_id`, `retention_item_id`, `value`) VALUES ((select id from retention_group where name ='5000' and coverage_group_id=(select id from coverage_group where name ='Sexual Misconduct and Molestation Liability' and product_version_id=(select id from product_version where name ='SMML_20180101'))), (select id from retention_item where name = 'Sexual Misconduct and Molestation Liability' and coverage_group_item_id=(select id from coverage_group_item where name = 'Sexual Misconduct and Molestation Liability' and coverage_group_id = (select id from coverage_group where name ='Sexual Misconduct and Molestation Liability' and product_version_id=(select id from product_version where name ='SMML_20180101')))), 5000);"""

'endts_forms_sql = "INSERT INTO `auth`.`form` (`number`, `url`, `type`, `name`) VALUES ('E1861SMH-1216', 'https://www.nasinsurance.com/var/documents/E1861SMH-1216.pdf', 'endorsement' , 'Additional Insured Amendatory - No Primary, Non Contributory');"

'endts_sql = "INSERT INTO `endorsement` (`conditions`, `sequence`, `is_default`, `is_active`, `is_manuscript`, `product_version_id`, `form_id`) VALUES ('', 1, 0, 1, 0, (select id from product_version where name ='SMML_20180101'), (select id from form where number ='E1861SMH-1216'));"

'subj_sql = "INSERT INTO `subjectivity` (`description`, `conditions`, `sequence`, `product_version_id`, `is_due_before_binding`, `is_default`, `is_active`) VALUES ('Completed Warranty Statement (A1861WS-0414)', '', 1, (select id from product_version where name ='SMML_20180101'), 1, 1, 1);"

coverage_group_sql = "INSERT INTO `coverage_group` (`name`, `is_default`, `is_primary`, `product_version_id`) VALUE ('COVERAGEGROUPNAME', ISDEFAULTFLAG, ISPRIMARYFLAG, (select id from product_version where name ='PRODUCTVERSIONNAME'));"

coverage_item_sql = "INSERT INTO `coverage_group_item` (`name`, `number`,  `is_included_by_endorsement`, `is_enhancement`, `coverage_group_id`,`limit_type_id`) VALUES ('COVERAGEITEMNAME', COVERAGEITEMNUMBER, BYENDT, ISENHANCEMENT, (select id from coverage_group where name ='COVERAGEGROUPNAME' and product_version_id=(select id from product_version where name ='PRODUCTVERSIONNAME')), (select id from limit_type where type = 'LIMITTYPE'));"

coverage_sub_item_sql = "INSERT INTO coverage_group_sub_item (`name`, `number`, `coverage_group_item_id`,`limit_type_id`) VALUES ('COVERAGESUBITEMNAME', COVERAGESUBITEMNUMBER, (select id from coverage_group_item where name ='COVERAGEITEMNAME' and coverage_group_id =(select id from coverage_group where name ='COVERAGEGROUPNAME' and product_version_id=(select id from product_version where name ='PRODUCTVERSIONNAME'))), (select id from limit_type where type = 'LIMITTYPE'));"

'mm_template_sql = "INSERT INTO `auth`.`template` (`name`, `type`, `reportingcloud_template_name`) VALUES ('1', '1', '1');"

'mm_sql = "INSERT INTO `auth`.`marketing_material` (`product_version_id`, `business_class_id`, `template_id`) VALUES ('`', '`', '`');"

policy_form_sql = "INSERT INTO `form` (`name`, `number`, `url`, `type`) SELECT 'FORMNAME','FORMNUMBER', 'FORMURL', 'policy' FROM `policy` WHERE NOT EXISTS (SELECT * FROM `form` WHERE `name`='FORMNAME' and `number`='FORMNUMBER' and `url`='FORMURL' and `type`='policy') LIMIT 1;"

'end of sql section

Dim myFile As String
myFile = CurDir() & "\product_configuration.sql"
Open myFile For Output As #1


Print #1, "SET autocommit = 0;"
Print #1, "start transaction;"
Print #1, "SET NAMES 'utf8';"
Print #1, "SET CHARACTER SET utf8;"


'policy form
tabname = "Prd Ver"
Sheets(tabname).Select
totalrows = Sheets(tabname).UsedRange.Rows.Count
x = 2
Do While x <= totalrows
   

    'Get Cell Values by Column - Prd Ver Tab
    cellProductName = Trim(Range("B" & x).Value)
    cellProductVersion = Trim(Range("D" & x).Value)
    cellFormName = Trim(Range("E" & x).Value)
    cellFormNumber = Trim(Range("F" & x).Value)
    cellFormURL = Trim(Range("G" & x).Value)
    cellLineCode = Trim(Range("H" & x).Value)
    cellContract = Trim(Range("I" & x).Value)
    cellHRCode = Trim(Range("J" & x).Value)
    cellPIF = Trim(Range("K" & x).Value)
    cellOptionalEnhancement = Trim(Range("T" & x).Value)
    'cellDefaultMEP needs to be added
    
    policy_form_sql_complete = policy_form_sql
    policy_form_sql_complete = Strings.Replace(policy_form_sql_complete, "FORMNAME", cellFormName)
    policy_form_sql_complete = Strings.Replace(policy_form_sql_complete, "FORMNUMBER", cellFormNumber)
    policy_form_sql_complete = Strings.Replace(policy_form_sql_complete, "FORMURL", cellFormURL)
    

    Debug.Print (policy_form_sql_complete)
    Print #1, policy_form_sql_complete
    
  'product version commission
  'Get Cell Values by Column - product version commission
     cellcomm1 = Trim(Range("L" & x).Value)
     cellcomm2 = Trim(Range("M" & x).Value)
     cellcomm3 = Trim(Range("N" & x).Value)
     cellcomm4 = Trim(Range("O" & x).Value)
     cellcomm5 = Trim(Range("P" & x).Value)
     cellcomm6 = Trim(Range("Q" & x).Value)
     cellcomm7 = Trim(Range("R" & x).Value)
     cellcomm8 = Trim(Range("S" & x).Value)
     product_version_commission_sql_complete = Strings.Replace(product_version_commission_sql, "COMM1", cellcomm1)
     product_version_commission_sql_complete = Strings.Replace(product_version_commission_sql_complete, "COMM2", cellcomm2)
     product_version_commission_sql_complete = Strings.Replace(product_version_commission_sql_complete, "COMM3", cellcomm3)
     product_version_commission_sql_complete = Strings.Replace(product_version_commission_sql_complete, "COMM4", cellcomm4)
     product_version_commission_sql_complete = Strings.Replace(product_version_commission_sql_complete, "COMM5", cellcomm5)
     product_version_commission_sql_complete = Strings.Replace(product_version_commission_sql_complete, "COMM6", cellcomm6)
     product_version_commission_sql_complete = Strings.Replace(product_version_commission_sql_complete, "COMM7", cellcomm7)
     product_version_commission_sql_complete = Strings.Replace(product_version_commission_sql_complete, "COMM8", cellcomm8)
     
     Debug.Print (product_version_commission_sql_complete)
     Print #1, product_version_commission_sql_complete
     
 'product version
    product_version_sql = "INSERT INTO `product_version` (`name`, `default_policy_issuance_fee`, `optional_enhancements`, `product_id`, `policy_form_id`, `contract_id`, `line_code`, `hr_code`, `product_version_commission_id`, `default_minimum_earned_premium`) VALUES ('PRODUCTVERSIONNAME', DEFAULTPIF, 'OPTIONALENHANCEMENTS', (select id from product where name = 'PRODUCTNAME'),(select id from form where number ='POLICYFORMNUMBER'),(select id from contract where code='CONTRACTNUMBER'), 'LINECODE', 'HRCODE', (select max(id) from product_version_commission), 25);"
    product_version_sql_complete = product_version_sql
    product_version_sql_complete = Strings.Replace(product_version_sql_complete, "PRODUCTVERSIONNAME", cellProductVersion)
    product_version_sql_complete = Strings.Replace(product_version_sql_complete, "PRODUCTNAME", cellProductName)
    product_version_sql_complete = Strings.Replace(product_version_sql_complete, "POLICYFORMNUMBER", cellFormNumber)
    product_version_sql_complete = Strings.Replace(product_version_sql_complete, "LINECODE", cellLineCode)
    product_version_sql_complete = Strings.Replace(product_version_sql_complete, "PRODUCTID", cellFormName)
    product_version_sql_complete = Strings.Replace(product_version_sql_complete, "CONTRACTNUMBER", cellContract)
    product_version_sql_complete = Strings.Replace(product_version_sql_complete, "LINECODE", cellFormName)
    product_version_sql_complete = Strings.Replace(product_version_sql_complete, "HRCODE", cellHRCode)
    product_version_sql_complete = Strings.Replace(product_version_sql_complete, "OPTIONALENHANCEMENTS", cellOptionalEnhancement)
    product_version_sql_complete = Strings.Replace(product_version_sql_complete, "DEFAULTPIF", cellPIF)

    Debug.Print (product_version_sql_complete)
     Print #1, product_version_sql_complete
     
     
    x = x + 1
Loop





Sheets("CvgGrps & Itms").Select
totalrows = Sheets("CvgGrps & Itms").UsedRange.Rows.Count
Dim new_product_version_flag As Boolean
Dim primary_coverage_group, default_coverage_group As Integer



x = 2
Do While x <= totalrows
    new_product_version_flag = False
    
    'Get Cell Values by Column - CvgGrps & Itms Tab
    cellProductName = Trim(Range("B" & x).Value)
    cellPriorProductName = Trim(Range("B" & x - 1).Value)
    cellProductVersion = Trim(Range("D" & x).Value)
    cellPriorProductVersion = Trim(Range("D" & x - 1).Value)
    cellCoverageGroupName = Trim(Range("F" & x).Value)
    cellPriorCoverageGroupName = Trim(Range("F" & x - 1).Value)
    cellCoverageItemName = Trim(Range("K" & x).Value)
    cellPriorCoverageItemName = Trim(Range("K" & x - 1).Value)
    cellRetentionItemName = Trim(Range("R" & x).Value)
    cellPriorRetentionItemName = Trim(Range("R" & x - 1).Value)
    cellRetentionType = Trim(Range("S" & x).Value)
    cellPriorRetentionType = Trim(Range("S" & x - 1).Value)
    cellRetentionValueType = Trim(Range("T" & x).Value)
    cellCoverageItemNumber = Trim(Range("J" & x).Value)
    cellCoverageItemByEndt = Trim(Range("M" & x).Value)
    cellCoverageItemIsEnhancement = Trim(Range("N" & x).Value)
    cellPrimaryCoverageGroup = Trim(Range("G" & x).Value)
    cellDefaultCoverageGroup = Trim(Range("H" & x).Value)
    cellParentCoverageItem = Trim(Range("L" & x).Value)
    cellRetentionItemSequence = Trim(Range("Q" & x).Value)
    cellLimitType = Trim(Range("O" & x).Value)
    cellSizeFactorName = Trim(Range("E" & x).Value)
    
    'end excel cell values
    
    'product versions
    If cellProductName <> cellPriorProductName Or cellProductVersion <> cellPriorProductVersion Then
        new_product_version_flag = True
        'get all the data we need
        product_name = cellProductName
        product_version = cellProductVersion
        
        
        If product_version = "" Then product_version = "*" & product_name
        
        'build the sql
        'to do: product rating mechanism
        'to to: export to file
        product_name = Strings.Replace(product_name, "'", "\'")
        product_version = Strings.Replace(product_version, "'", "\'")
        product_name = Strings.Replace(product_name, "’", "\'")
        product_version = Strings.Replace(product_version, "’", "\'")
        size_factor_name = cellSizeFactorName

        'product_version_sql_complete = Strings.Replace(product_version_sql, "PRODUCTVERSIONNAME", product_version)
        'product_version_sql_complete = Strings.Replace(product_version_sql_complete, "PRODUCTNAME", product_name)
        'product_version_sql_complete = Strings.Replace(product_version_sql_complete, Chr(34), "")
       
        'product_version_criteria_sql_complete = Strings.Replace(product_version_criteria_sql, "PRODUCTVERSIONNAME", product_version)
        'product_version_criteria_sql_complete = Strings.Replace(product_version_criteria_sql_complete, "SIZEFACTORNAME", size_factor_name)
        'product_version_criteria_sql_complete = Strings.Replace(product_version_criteria_sql_complete, "PRODUCTNAME", product_name)
        'product_version_criteria_sql_complete = Strings.Replace(product_version_criteria_sql_complete, Chr(34), "")

        'Debug.Print (product_version_sql_complete)
        'Print #1, product_version_sql_complete
        'Print #1, product_version_criteria_sql_complete
    End If
    
    'coverage groups
    If cellProductName <> cellPriorProductName Or cellProductVersion <> cellPriorProductVersion Or cellCoverageGroupName <> cellPriorCoverageGroupName Then
        
        product_name = cellProductName
        coverage_group = cellCoverageGroupName
        product_version = cellProductVersion
        
        If coverage_group <> "" Then
            primary_coverage_group = cellPrimaryCoverageGroup
            default_coverage_group = cellDefaultCoverageGroup
           
        Else
            coverage_group = "*" & product_name
            primary_coverage_group = 1
            default_coverage_group = 1
        End If
        
        product_name = Strings.Replace(product_name, "'", "\'")
        product_version = Strings.Replace(product_version, "'", "\'")

        coverage_group = Strings.Replace(coverage_group, "'", "\'")
        coverage_group = Strings.Replace(coverage_group, "’", "\'")
        coverage_group = Strings.Replace(coverage_group, Chr(13), "")
        coverage_group = Strings.Replace(coverage_group, Chr(10), "")
                
        coverage_group_sql_complete = Strings.Replace(coverage_group_sql, "PRODUCTVERSIONNAME", product_version)
        coverage_group_sql_complete = Strings.Replace(coverage_group_sql_complete, "COVERAGEGROUPNAME", coverage_group)
        coverage_group_sql_complete = Strings.Replace(coverage_group_sql_complete, "ISPRIMARYFLAG", primary_coverage_group)
        coverage_group_sql_complete = Strings.Replace(coverage_group_sql_complete, "ISDEFAULTFLAG", default_coverage_group)
        coverage_group_sql_complete = Strings.Replace(coverage_group_sql_complete, Chr(34), "")
        
        Debug.Print (coverage_group_sql_complete)
        Print #1, coverage_group_sql_complete
        
    End If
    
    
    'coverage item limit types
      

    limit_type = cellLimitType
    
    If limit_type = "Notified Individuals Each Claim" Then 'temporary hack - add the configuration field later
        limit_value_type = "integer"
    Else
    limit_value_type = "currency"
    End If
    
    limit_type_sql = "INSERT INTO `limit_type` (`type`, `value_type`) SELECT 'LIMITTYPE', 'LIMITVALUETYPE' FROM `limit_type` WHERE NOT EXISTS (SELECT * FROM `limit_type` WHERE `type`='LIMITTYPE' AND `value_type`='LIMITVALUETYPE') LIMIT 1;"
    limit_type_sql_complete = limit_type_sql
    limit_type_sql_complete = Strings.Replace(limit_type_sql_complete, "LIMITTYPE", limit_type)
    limit_type_sql_complete = Strings.Replace(limit_type_sql_complete, "LIMITVALUETYPE", limit_value_type)

    Print #1, limit_type_sql_complete
    
    'coverage items
    If cellProductName <> cellPriorProductName Or cellProductVersion <> cellPriorProductVersion Or cellCoverageGroupName <> cellPriorCoverageGroupName Or cellCoverageItemName <> cellPriorCoverageItemName And cellParentCoverageItem = "" Then
        
        product_name = cellProductName
        coverage_group = cellCoverageGroupName
        coverage_item = cellCoverageItemName
        product_version = cellProductVersion
        limit_type = cellLimitType
        
        If coverage_item <> "" Then
            coverage_item_number = cellCoverageItemNumber
            coverage_item_by_endt = cellCoverageItemByEndt
             coverage_item_is_enhancement = cellCoverageItemIsEnhancement
        Else
            coverage_item = "*" & product_name
            coverage_item_number = 1
            coverage_item_by_endt = 0
             coverage_item_is_enhancement = 0
            
        End If
        
        product_name = Strings.Replace(product_name, "'", "\'")
        product_version = Strings.Replace(product_version, "'", "\'")
        coverage_group = Strings.Replace(coverage_group, "'", "\'")
        coverage_group = Strings.Replace(coverage_group, "’", "\'")
        coverage_item = Strings.Replace(coverage_item, Chr(10), "")
        coverage_item = Strings.Replace(coverage_item, Chr(13), "")
        coverage_item = Strings.Replace(coverage_item, "'", "\'")
        coverage_item = Strings.Replace(coverage_item, "’", "\’")
        
        coverage_item_sql_complete = Strings.Replace(coverage_item_sql, "PRODUCTVERSIONNAME", product_version)
        coverage_item_sql_complete = Strings.Replace(coverage_item_sql_complete, "COVERAGEGROUPNAME", coverage_group)
        coverage_item_sql_complete = Strings.Replace(coverage_item_sql_complete, "COVERAGEITEMNAME", coverage_item)
        coverage_item_sql_complete = Strings.Replace(coverage_item_sql_complete, "LIMITTYPE", limit_type)
        coverage_item_sql_complete = Strings.Replace(coverage_item_sql_complete, "COVERAGEITEMNUMBER", coverage_item_number)
        coverage_item_sql_complete = Strings.Replace(coverage_item_sql_complete, "BYENDT", coverage_item_by_endt)
        coverage_item_sql_complete = Strings.Replace(coverage_item_sql_complete, "ISENHANCEMENT", coverage_item_is_enhancement)
        coverage_item_sql_complete = Strings.Replace(coverage_item_sql_complete, Chr(34), "")

        If coverage_item <> cellPriorCoverageItemName Then
            Debug.Print (coverage_item_sql_complete)
            Print #1, coverage_item_sql_complete
        End If
        

        
    End If
    
    'coverage sub-items
    If cellParentCoverageItem <> "" Then
    
        'If cellProductName <> cellPriorProductName Or cellProductVersion <> cellPriorCoverageGroupName Or cellCoverageGroupName <> cellPriorCoverageGroupName Or cellCoverageItemName <> cellPriorCoverageItemName And Range("N" & x).Value = "" Then
            
            
            product_name = cellProductName
            coverage_group = cellCoverageGroupName
            coverage_item = cellParentCoverageItem
            product_version = cellProductVersion
            coverage_sub_item_name = cellCoverageItemName
            coverage_sub_item_number = cellCoverageItemNumber
            limit_type = cellLimitType
            
            product_name = Strings.Replace(product_name, "'", "\'")
            product_version = Strings.Replace(product_version, "'", "\'")
            coverage_group = Strings.Replace(coverage_group, "'", "\'")
            coverage_item = Strings.Replace(coverage_item, Chr(10), "")
            coverage_item = Strings.Replace(coverage_item, Chr(13), "")
            coverage_item = Strings.Replace(coverage_item, "'", "\'")
            coverage_group = Strings.Replace(coverage_group, "’", "\'")
            coverage_item = Strings.Replace(coverage_item, "’", "\'")
        
            coverage_sub_item_name = Strings.Replace(coverage_sub_item_name, Chr(10), "")
            coverage_sub_item_name = Strings.Replace(coverage_sub_item_name, Chr(13), "")
            coverage_sub_item_name = Strings.Replace(coverage_sub_item_name, "'", "\'")
            
            coverage_sub_item_sql_complete = Strings.Replace(coverage_sub_item_sql, "PRODUCTVERSIONNAME", product_version)
            coverage_sub_item_sql_complete = Strings.Replace(coverage_sub_item_sql_complete, "COVERAGEGROUPNAME", coverage_group)
            coverage_sub_item_sql_complete = Strings.Replace(coverage_sub_item_sql_complete, "LIMITTYPE", limit_type)
            coverage_sub_item_sql_complete = Strings.Replace(coverage_sub_item_sql_complete, "COVERAGEITEMNAME", coverage_item)
            coverage_sub_item_sql_complete = Strings.Replace(coverage_sub_item_sql_complete, "COVERAGESUBITEMNAME", coverage_sub_item_name)
            coverage_sub_item_sql_complete = Strings.Replace(coverage_sub_item_sql_complete, "COVERAGESUBITEMNUMBER", coverage_sub_item_number)
            coverage_sub_item_sql_complete = Strings.Replace(coverage_sub_item_sql_complete, Chr(34), "")
    
            'If coverage_item <> cellPriorCoverageItemName Then
                  Debug.Print (coverage_sub_item_sql_complete)
                    Print #1, coverage_sub_item_sql_complete
            'End If
            
    
            
        'End If
        
    End If
    
   ' Debug.Print (cellRetentionItemName)
    
    'Debug.Print (IsEmpty(cellRetentionItemName))
    
    'Debug.Print (IsEmpty(cellRetentionType))
    
  
    If (cellProductName <> cellPriorProductName Or cellProductVersion <> cellPriorProductVersion Or cellCoverageGroupName <> cellPriorCoverageGroupName Or cellCoverageItemName <> cellPriorCoverageItemName Or cellRetentionItemName <> cellPriorRetentionItemName Or cellRetentionType <> cellPriorRetentionType) And (IsEmpty(cellRetentionItemName) = False Or IsEmpty(cellRetentionType) = False) Then
        
         'retention types
        If cellRetentionType <> "" Then
            retention_type_sql = "INSERT INTO `retention_item_type` (`name`, `value_type`) SELECT 'RETENTIONTYPENAME', 'RETENTIONVALUETYPE' FROM `retention_item_type` WHERE NOT EXISTS (SELECT * FROM `retention_item_type` WHERE `name`='RETENTIONTYPENAME' AND `value_type`='RETENTIONVALUETYPE') LIMIT 1;"
            retention_type_sql_complete = retention_type_sql
            retention_type_sql_complete = Strings.Replace(retention_type_sql_complete, "RETENTIONTYPENAME", cellRetentionType)
            retention_type_sql_complete = Strings.Replace(retention_type_sql_complete, "RETENTIONVALUETYPE", cellRetentionValueType)
        
            Print #1, retention_type_sql_complete
        End If
    

     'retention items
     
        product_name = cellProductName
        coverage_group = cellCoverageGroupName
        coverage_item = cellCoverageItemName
        product_version = cellProductVersion
        retention_item = cellRetentionItemName
        retention_item_type = cellRetentionType
        retention_item_sequence = cellRetentionItemSequence
        
        
        If product_name = "" Then
            MsgBox "Product name missing from " & tabname & " row " & x
            Close #1
            Exit Sub
        End If
        
        If coverage_group = "" Then
            MsgBox "Coverage Group missing from " & tabname & " row " & x
            Close #1
            Exit Sub
        End If
        
        If coverage_item = "" Then
            MsgBox "Coverage Item missing from " & tabname & " row " & x
            Close #1
            Exit Sub
        End If
        
        If retention_item <> "" Then
           ' If retention_item_type = "" Then
           '     MsgBox "retention_item_type missing from " & tabname & " row " & x
           '     Close #1
           '     Exit Sub
           ' End If
        
            If retention_item_sequence = "" Then
                MsgBox "Retention Item Sequence missing from " & tabname & " row " & x
                Close #1
                Exit Sub
            End If
        End If
        
       
        
     If retention_item <> "" Then
     
        product_name = Strings.Replace(product_name, "'", "\'")
        product_version = Strings.Replace(product_version, "'", "\'")
        coverage_group = Strings.Replace(coverage_group, "'", "\'")
        coverage_group = Strings.Replace(coverage_group, "’", "\'")
        coverage_item = Strings.Replace(coverage_item, Chr(10), "")
        coverage_item = Strings.Replace(coverage_item, Chr(13), "")
        coverage_item = Strings.Replace(coverage_item, "'", "\'")
        coverage_item = Strings.Replace(coverage_item, "’", "\’")
        
    
    
    
        retention_item_sql_complete = Strings.Replace(retention_items_sql, "PRODUCTVERSIONNAME", product_version)
        retention_item_sql_complete = Strings.Replace(retention_item_sql_complete, "COVERAGEGROUPNAME", coverage_group)
        retention_item_sql_complete = Strings.Replace(retention_item_sql_complete, "COVERAGEITEMNAME", coverage_item)
        retention_item_sql_complete = Strings.Replace(retention_item_sql_complete, "RETENTIONITEMNAME", retention_item)
        retention_item_sql_complete = Strings.Replace(retention_item_sql_complete, "RETENTIONITEMTYPE", retention_item_type)
        retention_item_sql_complete = Strings.Replace(retention_item_sql_complete, "RETENTIONVALUETYPE", cellRetentionValueType)
        retention_item_sql_complete = Strings.Replace(retention_item_sql_complete, "RETENTIONITEMSEQUENCE", retention_item_sequence)
        retention_item_sql_complete = Strings.Replace(retention_item_sql_complete, Chr(34), "")
        
        Debug.Print (retention_item_sql_complete)
        Print #1, retention_item_sql_complete

    End If

        
    End If
    
    
    
    'endts
    
    
    
    'subjectivities
    
    
    
    'marketing material
    
        
 
    
    x = x + 1
Loop


'limit groups
tabname = "Lmt Grps"
Sheets(tabname).Select
totalrows = Sheets(tabname).UsedRange.Rows.Count
x = 2
Do While x <= totalrows

    cellProductVersion = Trim(Range("D" & x).Value)
    cellCoverageGroupName = Trim(Range("F" & x).Value)
    cellLimitGroup = Trim(Range("H" & x).Value)
    cellLimitGroupValue = Trim(Range("I" & x).Value)
    
    If cellProductVersion = "" Then
            MsgBox "Product Version missing from " & tabname & " row " & x
            Close #1
            Exit Sub
    End If
        
    If cellCoverageGroupName = "" Then
            MsgBox "Coverage Group Name missing from " & tabname & " row " & x
            Close #1
            Exit Sub
    End If
    
    If cellLimitGroup = "" Then
            MsgBox "Limit Group missing from " & tabname & " row " & x
            Close #1
            Exit Sub
    End If
    
    If cellLimitGroupValue = "" Then
            MsgBox "Limit Group Value missing from " & tabname & " row " & x
            Close #1
            Exit Sub
    End If
        
    limit_group_sql_complete = limit_group_sql
    limit_group_sql_complete = Strings.Replace(limit_group_sql_complete, "COVERAGEGROUPNAME", cellCoverageGroupName)
    limit_group_sql_complete = Strings.Replace(limit_group_sql_complete, "PRODUCTVERSIONNAME", cellProductVersion)
    limit_group_sql_complete = Strings.Replace(limit_group_sql_complete, "LIMITGROUPNAME", cellLimitGroup)
    limit_group_sql_complete = Strings.Replace(limit_group_sql_complete, "LIMITGROUPVALUE", cellLimitGroupValue)
    Debug.Print (limit_group_sql_complete)
    Print #1, limit_group_sql_complete
    
    
    'policy_form_sql_complete = policy_form_sql
    'policy_form_sql_complete = Strings.Replace(policy_form_sql_complete, "FORMNAME", cellFormName)
    'policy_form_sql_complete = Strings.Replace(policy_form_sql_complete, "FORMNUMBER", cellFormNumber)
    'policy_form_sql_complete = Strings.Replace(policy_form_sql_complete, "FORMURL", cellFormURL)
     'Print #1, policy_form_sql_complete
    x = x + 1
Loop

'agg limit groups
tabname = "Agg Lmt Grps"
Sheets(tabname).Select
totalrows = Sheets(tabname).UsedRange.Rows.Count
x = 2
Do While x <= totalrows

    cellProductVersion = Trim(Range("D" & x).Value)
    cellCoverageGroupName = Trim(Range("F" & x).Value)
    cellLimitGroup = Trim(Range("H" & x).Value)
    cellLimitGroupValue = Trim(Range("I" & x).Value)
    
    If cellProductVersion = "" Then
            MsgBox "Product Version missing from " & tabname & " row " & x
            Close #1
            Exit Sub
    End If
        
    If cellCoverageGroupName = "" Then
            MsgBox "Coverage Group Name missing from " & tabname & " row " & x
            Close #1
            Exit Sub
    End If
    
    If cellLimitGroup = "" Then
            MsgBox "Limit Group missing from " & tabname & " row " & x
            Close #1
            Exit Sub
    End If
    
    If cellLimitGroupValue = "" Then
            MsgBox "Limit Group Value missing from " & tabname & " row " & x
            Close #1
            Exit Sub
    End If
    
    aggregate_limit_group_sql = "INSERT INTO `aggregate_limit_group` (`coverage_group_id`, `name`,  `value`)" & _
" VALUES ((select id from coverage_group where name ='COVERAGEGROUPNAME' and product_version_id=(select id from product_version where name ='PRODUCTVERSIONNAME')), 'AGGLIMITGROUPNAME',  AGGLIMITGROUPVALUE);"


    limit_group_sql_complete = aggregate_limit_group_sql
    limit_group_sql_complete = Strings.Replace(limit_group_sql_complete, "COVERAGEGROUPNAME", cellCoverageGroupName)
    limit_group_sql_complete = Strings.Replace(limit_group_sql_complete, "PRODUCTVERSIONNAME", cellProductVersion)
    limit_group_sql_complete = Strings.Replace(limit_group_sql_complete, "AGGLIMITGROUPNAME", cellLimitGroup)
    limit_group_sql_complete = Strings.Replace(limit_group_sql_complete, "AGGLIMITGROUPVALUE", cellLimitGroupValue)
    Debug.Print (limit_group_sql_complete)
    Print #1, limit_group_sql_complete
    
    
    'policy_form_sql_complete = policy_form_sql
    'policy_form_sql_complete = Strings.Replace(policy_form_sql_complete, "FORMNAME", cellFormName)
    'policy_form_sql_complete = Strings.Replace(policy_form_sql_complete, "FORMNUMBER", cellFormNumber)
    'policy_form_sql_complete = Strings.Replace(policy_form_sql_complete, "FORMURL", cellFormURL)
     'Print #1, policy_form_sql_complete
    x = x + 1
Loop

'Cvg Item Limit Values
tabname = "Cvg Itm Lmts"
Sheets(tabname).Select
totalrows = Sheets(tabname).UsedRange.Rows.Count
x = 2
Do While x <= totalrows

    cellProductVersion = Trim(Range("D" & x).Value)
    cellCoverageGroupName = Trim(Range("F" & x).Value)
    cellCoverageItemName = Trim(Range("I" & x).Value)
    cellParentCoverageItemName = Trim(Range("J" & x).Value)
    cellLimitGroup = Trim(Range("M" & x).Value)
    cellLimitValue = Trim(Range("O" & x).Value)
    
    If cellProductVersion = "" Then
            MsgBox "Product Version missing from " & tabname & " row " & x
            Close #1
            Exit Sub
    End If
    
    If cellCoverageGroupName = "" Then
            MsgBox "Coverage Group Name missing from " & tabname & " row " & x
            Close #1
            Exit Sub
    End If
    

    If cellLimitGroup = "" Then
            MsgBox "Limit Group missing from " & tabname & " row " & x
            Close #1
            Exit Sub
    End If

    If cellLimitValue = "" Then
            MsgBox "Limit Value missing from " & tabname & " row " & x
            Close #1
            Exit Sub
    End If

    coverage_item_limits_sql = "INSERT INTO limit_group_coverage_group_item (limit_group_id, coverage_group_item_id, value) VALUES " & _
"((select id from limit_group where name = 'LIMITGROUPNAME' and coverage_group_id=(select id from coverage_group where name ='COVERAGEGROUPNAME' and product_version_id=(select id from product_version where name ='PRODUCTVERSIONNAME'))),(select id from coverage_group_item where name ='COVERAGEITEMNAME' and coverage_group_id =(select id from coverage_group where name ='COVERAGEGROUPNAME' and product_version_id=(select id from product_version where name ='PRODUCTVERSIONNAME'))), 'LIMITVALUE');"

coverage_sub_item_limits_sql = "INSERT INTO limit_group_coverage_group_sub_item (limit_group_id, coverage_group_sub_item_id, value) VALUES " & _
"((select id from limit_group where name = 'LIMITGROUPNAME' and coverage_group_id=(select id from coverage_group where name ='COVERAGEGROUPNAME' and product_version_id=(select id from product_version where name ='PRODUCTVERSIONNAME'))),(select id from coverage_group_sub_item where name ='COVERAGESUBITEMNAME' and coverage_group_item_id in (select id from coverage_group_item where name ='COVERAGEITEMNAME' and coverage_group_id =(select id from coverage_group where name ='COVERAGEGROUPNAME' and product_version_id=(select id from product_version where name ='PRODUCTVERSIONNAME')))), 'LIMITVALUE');"

If cellParentCoverageItemName = "" Then
    coverage_item_limits_sql_complete = coverage_item_limits_sql
    coverage_item_limits_sql_complete = Strings.Replace(coverage_item_limits_sql_complete, "COVERAGEGROUPNAME", cellCoverageGroupName)
    coverage_item_limits_sql_complete = Strings.Replace(coverage_item_limits_sql_complete, "PRODUCTVERSIONNAME", cellProductVersion)
    coverage_item_limits_sql_complete = Strings.Replace(coverage_item_limits_sql_complete, "COVERAGEITEMNAME", cellCoverageItemName)
    coverage_item_limits_sql_complete = Strings.Replace(coverage_item_limits_sql_complete, "LIMITGROUPNAME", cellLimitGroup)
    coverage_item_limits_sql_complete = Strings.Replace(coverage_item_limits_sql_complete, "LIMITGROUPVALUE", cellLimitGroupValue)
    coverage_item_limits_sql_complete = Strings.Replace(coverage_item_limits_sql_complete, "LIMITVALUE", cellLimitValue)

    Debug.Print (coverage_item_limits_sql_complete)
    Print #1, coverage_item_limits_sql_complete
Else
    coverage_sub_item_limits_sql_complete = coverage_sub_item_limits_sql
    coverage_sub_item_limits_sql_complete = Strings.Replace(coverage_sub_item_limits_sql_complete, "COVERAGEGROUPNAME", cellCoverageGroupName)
    coverage_sub_item_limits_sql_complete = Strings.Replace(coverage_sub_item_limits_sql_complete, "PRODUCTVERSIONNAME", cellProductVersion)
    coverage_sub_item_limits_sql_complete = Strings.Replace(coverage_sub_item_limits_sql_complete, "COVERAGESUBITEMNAME", cellCoverageItemName)
    coverage_sub_item_limits_sql_complete = Strings.Replace(coverage_sub_item_limits_sql_complete, "COVERAGEITEMNAME", cellParentCoverageItemName)
    coverage_sub_item_limits_sql_complete = Strings.Replace(coverage_sub_item_limits_sql_complete, "LIMITGROUPNAME", cellLimitGroup)
    coverage_sub_item_limits_sql_complete = Strings.Replace(coverage_sub_item_limits_sql_complete, "LIMITGROUPVALUE", cellLimitGroupValue)
    coverage_sub_item_limits_sql_complete = Strings.Replace(coverage_sub_item_limits_sql_complete, "LIMITVALUE", cellLimitValue)
    Debug.Print (coverage_sub_item_limits_sql_complete)
    Print #1, coverage_sub_item_limits_sql_complete
End If

    
    
    x = x + 1
Loop


'Cvg Item Agg Limit Values
tabname = "Cvg Itm Agg Lmts"
Sheets(tabname).Select
totalrows = Sheets(tabname).UsedRange.Rows.Count
x = 2
Do While x <= totalrows

    cellProductVersion = Trim(Range("D" & x).Value)
    cellCoverageGroupName = Trim(Range("F" & x).Value)
    cellCoverageItemName = Trim(Range("I" & x).Value)
    cellParentCoverageItemName = Trim(Range("J" & x).Value)
    cellLimitGroup = Trim(Range("M" & x).Value)
    cellLimitValue = Trim(Range("O" & x).Value)
    
    If cellProductVersion = "" Then
            MsgBox "Product Version missing from " & tabname & " row " & x
            Close #1
            Exit Sub
    End If
    
    If cellCoverageGroupName = "" Then
            MsgBox "Coverage Group Name missing from " & tabname & " row " & x
            Close #1
            Exit Sub
    End If
    

    If cellLimitGroup = "" Then
            MsgBox "Limit Group missing from " & tabname & " row " & x
            Close #1
            Exit Sub
    End If

    If cellLimitValue = "" Then
            MsgBox "Limit Value missing from " & tabname & " row " & x
            Close #1
            Exit Sub
    End If
    
coverage_item_aggregate_limits_sql = "INSERT INTO aggregate_limit_group_coverage_group_item (aggregate_limit_group_id, coverage_group_item_id, value) VALUES " & _
"((select id from aggregate_limit_group where name = 'LIMITGROUPNAME' and coverage_group_id=(select id from coverage_group where name ='COVERAGEGROUPNAME' and product_version_id=(select id from product_version where name ='PRODUCTVERSIONNAME'))), (select id from coverage_group_item where name ='COVERAGEITEMNAME' and coverage_group_id =(select id from coverage_group where name ='COVERAGEGROUPNAME' and product_version_id=(select id from product_version where name ='PRODUCTVERSIONNAME'))), 'LIMITVALUE');"

coverage_sub_item_aggregate_limits_sql = "INSERT INTO aggregate_limit_group_coverage_group_sub_item (aggregate_limit_group_id, coverage_group_sub_item_id, value) VALUES " & _
"((select id from aggregate_limit_group where name = 'LIMITGROUPNAME' and coverage_group_id=(select id from coverage_group where name ='COVERAGEGROUPNAME' and product_version_id=(select id from product_version where name ='PRODUCTVERSIONNAME'))), (select id from coverage_group_sub_item where name ='COVERAGESUBITEMNAME' and coverage_group_item_id in (select id from coverage_group_item where name ='COVERAGEITEMNAME' and coverage_group_id =(select id from coverage_group where name ='COVERAGEGROUPNAME' and product_version_id=(select id from product_version where name ='PRODUCTVERSIONNAME')))), 'LIMITVALUE');"

If cellParentCoverageItemName = "" Then
    coverage_item_limits_sql_complete = coverage_item_aggregate_limits_sql
    coverage_item_limits_sql_complete = Strings.Replace(coverage_item_limits_sql_complete, "COVERAGEGROUPNAME", cellCoverageGroupName)
    coverage_item_limits_sql_complete = Strings.Replace(coverage_item_limits_sql_complete, "PRODUCTVERSIONNAME", cellProductVersion)
    coverage_item_limits_sql_complete = Strings.Replace(coverage_item_limits_sql_complete, "COVERAGEITEMNAME", cellCoverageItemName)
    coverage_item_limits_sql_complete = Strings.Replace(coverage_item_limits_sql_complete, "LIMITGROUPNAME", cellLimitGroup)
    coverage_item_limits_sql_complete = Strings.Replace(coverage_item_limits_sql_complete, "LIMITGROUPVALUE", cellLimitGroupValue)
    coverage_item_limits_sql_complete = Strings.Replace(coverage_item_limits_sql_complete, "LIMITVALUE", cellLimitValue)
    Debug.Print (coverage_item_limits_sql_complete)
    Print #1, coverage_item_limits_sql_complete
Else
    coverage_sub_item_limits_sql_complete = coverage_sub_item_aggregate_limits_sql
    coverage_sub_item_limits_sql_complete = Strings.Replace(coverage_sub_item_limits_sql_complete, "COVERAGEGROUPNAME", cellCoverageGroupName)
    coverage_sub_item_limits_sql_complete = Strings.Replace(coverage_sub_item_limits_sql_complete, "PRODUCTVERSIONNAME", cellProductVersion)
    coverage_sub_item_limits_sql_complete = Strings.Replace(coverage_sub_item_limits_sql_complete, "COVERAGESUBITEMNAME", cellCoverageItemName)
    coverage_sub_item_limits_sql_complete = Strings.Replace(coverage_sub_item_limits_sql_complete, "COVERAGEITEMNAME", cellParentCoverageItemName)
    coverage_sub_item_limits_sql_complete = Strings.Replace(coverage_sub_item_limits_sql_complete, "LIMITGROUPNAME", cellLimitGroup)
    coverage_sub_item_limits_sql_complete = Strings.Replace(coverage_sub_item_limits_sql_complete, "LIMITGROUPVALUE", cellLimitGroupValue)
    coverage_sub_item_limits_sql_complete = Strings.Replace(coverage_sub_item_limits_sql_complete, "LIMITVALUE", cellLimitValue)
    Debug.Print (coverage_sub_item_limits_sql_complete)
    Print #1, coverage_sub_item_limits_sql_complete
End If

    
    
    x = x + 1
Loop



'retention groups
tabname = "Rtn Grps"
Sheets(tabname).Select
totalrows = Sheets(tabname).UsedRange.Rows.Count
x = 2
Do While x <= totalrows

    cellProductVersion = Trim(Range("A" & x).Value)
    cellCoverageGroupName = Trim(Range("B" & x).Value)
    cellRetentionGroup = Trim(Range("D" & x).Value)
    
    If cellProductVersion = "" Then
            MsgBox "Product Version missing from " & tabname & " row " & x
            Close #1
            Exit Sub
    End If
    
    If cellCoverageGroupName = "" Then
            MsgBox "Coverage Group Name missing from " & tabname & " row " & x
            Close #1
            Exit Sub
    End If
    
    If cellRetentionGroup = "" Then
            MsgBox "Retention Group missing from " & tabname & " row " & x
            Close #1
            Exit Sub
    End If
    
    retention_groups_sql = "INSERT INTO `retention_group` (`coverage_group_id`, `name`, `value`) VALUES ((select id from coverage_group where name ='COVERAGEGROUPNAME' and product_version_id=(select id from product_version where name ='PRODUCTVERSIONNAME')), 'RETENTIONGROUPNAME', 'RETENTIONGROUPVALUE');"



    retention_groups_sql_complete = retention_groups_sql
    retention_groups_sql_complete = Strings.Replace(retention_groups_sql_complete, "COVERAGEGROUPNAME", cellCoverageGroupName)
    retention_groups_sql_complete = Strings.Replace(retention_groups_sql_complete, "PRODUCTVERSIONNAME", cellProductVersion)
    retention_groups_sql_complete = Strings.Replace(retention_groups_sql_complete, "RETENTIONGROUPNAME", cellRetentionGroup)
    retention_groups_sql_complete = Strings.Replace(retention_groups_sql_complete, "RETENTIONGROUPVALUE", cellRetentionGroup)
    Debug.Print (retention_groups_sql_complete)
    Print #1, retention_groups_sql_complete
    
    x = x + 1
Loop


'retention item values
tabname = "Rtn Itm Vals"
Sheets(tabname).Select
totalrows = Sheets(tabname).UsedRange.Rows.Count
x = 2
Do While x <= totalrows

    cellProductVersion = Trim(Range("A" & x).Value)
    cellCoverageGroupName = Trim(Range("B" & x).Value)
    cellCoverageItemName = Trim(Range("C" & x).Value)
    cellRetentionItemName = Trim(Range("D" & x).Value)
    cellRetentionItemType = Trim(Range("E" & x).Value)
    cellRetentionValueType = Trim(Range("F" & x).Value)
    cellRetentionGroup = Trim(Range("G" & x).Value)
    cellRetentionValue = Trim(Range("I" & x).Value)
    
    
    If cellProductVersion = "" Then
            MsgBox "Product Version missing from " & tabname & " row " & x
            Close #1
            Exit Sub
    End If
    
    If cellCoverageGroupName = "" Then
            MsgBox "Coverage Group Name missing from " & tabname & " row " & x
            Close #1
            Exit Sub
    End If
    
    If cellCoverageItemName = "" Then
            MsgBox "Coverage Item Name missing from " & tabname & " row " & x
            Close #1
            Exit Sub
    End If
    
    If cellRetentionItemName = "" Then
            MsgBox "Retention Item Name missing from " & tabname & " row " & x
            Close #1
            Exit Sub
    End If
    
    
    If cellRetentionGroup = "" Then
            MsgBox "Retention Group missing from " & tabname & " row " & x
            Close #1
            Exit Sub
    End If
    
    If cellRetentionValue = "" Then
            MsgBox "Retention Value missing from " & tabname & " row " & x
            Close #1
            Exit Sub
    End If
    
    If cellRetentionValueType <> "" Then
        retention_item_values_sql = "INSERT INTO `retention_group_retention_item` (`retention_group_id`, `retention_item_id`, `value`) VALUES ((select id from retention_group where name ='RETENTIONGROUPNAME' and coverage_group_id=(select id from coverage_group where name ='COVERAGEGROUPNAME' and product_version_id=(select id from product_version where name ='PRODUCTVERSIONNAME'))), (select id from retention_item where name = 'RETENTIONITEMNAME' and retention_item_type_id = (select id from retention_item_type where name = 'RETENTIONITEMTYPE' and value_type='RETENTIONVALUETYPE') and coverage_group_item_id=(select id from coverage_group_item where name = 'COVERAGEITEMNAME' and coverage_group_id = (select id from coverage_group where name ='COVERAGEGROUPNAME' and product_version_id=(select id from product_version where name ='PRODUCTVERSIONNAME')))), RETENTIONVALUE);"
    Else
        retention_item_values_sql = "INSERT INTO `retention_group_retention_item` (`retention_group_id`, `retention_item_id`, `value`) VALUES ((select id from retention_group where name ='RETENTIONGROUPNAME' and coverage_group_id=(select id from coverage_group where name ='COVERAGEGROUPNAME' and product_version_id=(select id from product_version where name ='PRODUCTVERSIONNAME'))), (select id from retention_item where name = 'RETENTIONITEMNAME' and retention_item_type_id = (select id from retention_item_type where name = 'RETENTIONITEMTYPE') and coverage_group_item_id=(select id from coverage_group_item where name = 'COVERAGEITEMNAME' and coverage_group_id = (select id from coverage_group where name ='COVERAGEGROUPNAME' and product_version_id=(select id from product_version where name ='PRODUCTVERSIONNAME')))), RETENTIONVALUE);"
    End If

    retention_item_values_sql_complete = retention_item_values_sql
    retention_item_values_sql_complete = Strings.Replace(retention_item_values_sql_complete, "RETENTIONVALUETYPE", cellRetentionValueType)
    retention_item_values_sql_complete = Strings.Replace(retention_item_values_sql_complete, "COVERAGEGROUPNAME", cellCoverageGroupName)
    retention_item_values_sql_complete = Strings.Replace(retention_item_values_sql_complete, "COVERAGEITEMNAME", cellCoverageItemName)
    retention_item_values_sql_complete = Strings.Replace(retention_item_values_sql_complete, "PRODUCTVERSIONNAME", cellProductVersion)
    retention_item_values_sql_complete = Strings.Replace(retention_item_values_sql_complete, "RETENTIONGROUPNAME", cellRetentionGroup)
    retention_item_values_sql_complete = Strings.Replace(retention_item_values_sql_complete, "RETENTIONVALUE", cellRetentionValue)
    retention_item_values_sql_complete = Strings.Replace(retention_item_values_sql_complete, "RETENTIONITEMNAME", cellRetentionItemName)
    retention_item_values_sql_complete = Strings.Replace(retention_item_values_sql_complete, "RETENTIONITEMTYPE", cellRetentionItemType)
    retention_item_values_sql_complete = Strings.Replace(retention_item_values_sql_complete, "0TYPE", cellRetentionValueType) 'this is a possible fix for a weird issue that i can't explain.  need to take this out after figuring it out
     
    Debug.Print (retention_item_values_sql_complete)
    Print #1, retention_item_values_sql_complete
    
    x = x + 1
    
    Loop
    
  'product version criteria
  
tabname = "Prd Ver Criteria"
Sheets(tabname).Select
totalrows = Sheets(tabname).UsedRange.Rows.Count
x = 2
Do While x <= totalrows

    cellProductID = Trim(Range("A" & x).Value)
    cellProductName = Trim(Range("B" & x).Value)
    cellBusinessClassName = Trim(Range("G" & x).Value)
    cellNAICSCode = Trim(Range("H" & x).Value)
    cellEffectiveDateStart = Trim(Range("I" & x).Value)
    cellEffectiveDateStart = Format(cellEffectiveDateStart, "yyyy-mm-dd")
    cellEffectiveDateEnd = Trim(Range("J" & x).Value)
    cellEffectiveDateEnd = Format(cellEffectiveDateEnd, "yyyy-mm-dd")
    cellRatingMechanism = Trim(Range("E" & x).Value)
    cellProductVersion = Trim(Range("D" & x).Value)
    
    
    If cellProductID = "" Then
        MsgBox "cellProductID missing from " & tabname & " row " & x
        Close #1
        Exit Sub
    End If
    
    
    If cellProductName = "" Then
        MsgBox "cellProductName missing from " & tabname & " row " & x
        Close #1
        Exit Sub
    End If
    
    If cellBusinessClassName = "" Then
        MsgBox "cellBusinessClassName missing from " & tabname & " row " & x
        Close #1
        Exit Sub
    End If
    
    If cellNAICSCode = "" Then
        MsgBox "cellNAICSCode missing from " & tabname & " row " & x
        Close #1
        Exit Sub
    End If
    
    'rating_mechanism/size factor
    size_factor_sql = "INSERT INTO `size_factor` (`name`, `type`, `product_id`) SELECT 'SIZEFACTORNAME', 'integer', PRODUCTID FROM `size_factor` WHERE NOT EXISTS (SELECT name FROM `size_factor` WHERE `name`='SIZEFACTORNAME' and product_id = (select id from product where name='PRODUCTNAME')) LIMIT 1;"
    size_factor_sql_complete = size_factor_sql
    size_factor_sql_complete = Strings.Replace(size_factor_sql_complete, "SIZEFACTORNAME", cellRatingMechanism)
    size_factor_sql_complete = Strings.Replace(size_factor_sql_complete, "PRODUCTNAME", cellProductName)
    size_factor_sql_complete = Strings.Replace(size_factor_sql_complete, "BUSINESSCLASSNAME", cellBusinessClassName)
    size_factor_sql_complete = Strings.Replace(size_factor_sql_complete, "PRODUCTID", cellProductID)
   
    Debug.Print (size_factor_sql_complete)
    Print #1, size_factor_sql_complete
    
    naics_code_sql = "INSERT IGNORE INTO `naics_code` (`code`, `name`) VALUES ('NAICSCODE', 'NAICSCODE');"
    business_class_sql = "INSERT INTO `business_class` (`name`, `naics_code_id`) SELECT 'BUSINESSCLASSNAME', (select id from naics_code where code = 'NAICSCODE') FROM `business_class` WHERE NOT EXISTS (SELECT name, naics_code_id FROM `business_class` WHERE `name`='BUSINESSCLASSNAME') LIMIT 1;"
    'product_business_class_sql

    naics_code_sql_complete = naics_code_sql
    business_class_sql_complete = business_class_sql
    
    naics_code_sql_complete = Strings.Replace(naics_code_sql_complete, "NAICSCODE", cellNAICSCode)
    business_class_sql_complete = Strings.Replace(business_class_sql_complete, "BUSINESSCLASSNAME", cellBusinessClassName)
    business_class_sql_complete = Strings.Replace(business_class_sql_complete, "NAICSCODE", cellNAICSCode)

    Debug.Print (naics_code_sql_complete)
    Print #1, naics_code_sql_complete
    
    Debug.Print (business_class_sql_complete)
    Print #1, business_class_sql_complete
          
    
    product_business_class_sql = "INSERT INTO `product_business_class` (`product_id`, `business_class_id`) VALUES ((select id from product where name ='PRODUCTNAME'), (select id from business_class where name ='BUSINESSCLASSNAME'));"
    product_business_class_sql_complete = product_business_class_sql
    product_business_class_sql_complete = Strings.Replace(product_business_class_sql_complete, "PRODUCTNAME", cellProductName)
    product_business_class_sql_complete = Strings.Replace(product_business_class_sql_complete, "BUSINESSCLASSNAME", cellBusinessClassName)
    
    
    Debug.Print (product_business_class_sql_complete)
    Print #1, product_business_class_sql_complete
    
    product_version_criteria_sql = "INSERT INTO  product_version_criteria (`size_factor_id`, `effective_date_start`, `effective_date_end`, `business_class_id`, `product_version_id`) values ((select id from size_factor where name ='SIZEFACTORNAME' and product_id = (select id from product where name ='PRODUCTNAME')), '2018-01-01', '2020-01-01',(select id from business_class where name ='BUSINESSCLASSNAME'), (select id from product_version where name ='PRODUCTVERSIONNAME'));"
    product_version_criteria_sql_complete = product_version_criteria_sql
    product_version_criteria_sql_complete = Strings.Replace(product_version_criteria_sql_complete, "SIZEFACTORNAME", cellRatingMechanism)
    product_version_criteria_sql_complete = Strings.Replace(product_version_criteria_sql_complete, "PRODUCTNAME", cellProductName)
    product_version_criteria_sql_complete = Strings.Replace(product_version_criteria_sql_complete, "BUSINESSCLASSNAME", cellBusinessClassName)
    product_version_criteria_sql_complete = Strings.Replace(product_version_criteria_sql_complete, "PRODUCTVERSIONNAME", cellProductVersion)
   
   
    Debug.Print (product_version_criteria_sql_complete)
    Print #1, product_version_criteria_sql_complete
    

    
    x = x + 1
Loop

'clear dupes from product_business_class
Debug.Print (clear_duplicate_product_business_class_sql)
    Print #1, clear_duplicate_product_business_class_sql

'endorsements
tabname = "Endts"
Sheets(tabname).Select
totalrows = Sheets(tabname).UsedRange.Rows.Count
x = 2
Do While x <= totalrows

    cellProductVersion = Trim(Range("D" & x).Value)
    cellEndtSeq = Trim(Range("F" & x).Value)
    cellEndtName = Trim(Range("H" & x).Value)
    cellEndtNumber = Trim(Range("I" & x).Value)
    cellEndtURL = Trim(Range("J" & x).Value)
    cellEndtCondition = Trim(Range("L" & x).Value)
    cellEndtIsDefault = Trim(Range("M" & x).Value)
    cellEndtIsManuscript = Trim(Range("N" & x).Value)
    cellDefaultAddlInfo = Trim(Range("K" & x).Value)
    
    If cellProductVersion = "" Then
        MsgBox "Product Version missing from " & tabname & " row " & x
        Close #1
        Exit Sub
    End If
    
    If cellEndtSeq = "" Then
        MsgBox "cellEndtSeq missing from " & tabname & " row " & x
        Close #1
        Exit Sub
    End If
    
    If cellEndtName = "" Then
        MsgBox "cellEndtName missing from " & tabname & " row " & x
        Close #1
        Exit Sub
    End If
    
    cellEndtName = Strings.Replace(cellEndtName, "'", "\'")
    cellEndtCondition = Strings.Replace(cellEndtCondition, "'", "\'")
    cellDefaultAddlInfo = Strings.Replace(cellDefaultAddlInfo, "'", "\'")
    
    'endt forms
    endt_forms_sql = "INSERT INTO `form` (`name`, `number`, `url`, `type`) SELECT 'FORMNAME','FORMNUMBER', 'FORMURL', 'endorsement' FROM `form` WHERE NOT EXISTS (SELECT * FROM `form` WHERE `number`='FORMNUMBER' and `type`='endorsement') LIMIT 1;"
    endt_forms_sql_complete = endt_forms_sql
    endt_forms_sql_complete = Strings.Replace(endt_forms_sql_complete, "FORMNAME", cellEndtName)
    endt_forms_sql_complete = Strings.Replace(endt_forms_sql_complete, "FORMNUMBER", cellEndtNumber)
    endt_forms_sql_complete = Strings.Replace(endt_forms_sql_complete, "FORMURL", cellEndtURL)
   
    Debug.Print (endt_forms_sql_complete)
    Print #1, endt_forms_sql_complete
    

    
   'endts
    endt_sql = "INSERT INTO `endorsement` (`conditions`, `sequence`, `is_default`, `is_active`, `is_manuscript`, `product_version_id`, `form_id` ,`default_additional_info`) VALUES ('CONDITIONS', SEQUENCE, ISDEFAULT, 1, ISMANUSCRIPT, (select id from product_version where name ='PRODUCTVERSIONNAME'), (select id from form where number ='FORMNUMBER'),'DEFAULTADDITIONALINFO');"
    endt_sql_complete = endt_sql
    endt_sql_complete = Strings.Replace(endt_sql_complete, "CONDITIONS", cellEndtCondition)
    endt_sql_complete = Strings.Replace(endt_sql_complete, "FORMNUMBER", cellEndtNumber)
    endt_sql_complete = Strings.Replace(endt_sql_complete, "SEQUENCE", cellEndtSeq)
    endt_sql_complete = Strings.Replace(endt_sql_complete, "ISDEFAULT", cellEndtIsDefault)
    endt_sql_complete = Strings.Replace(endt_sql_complete, "ISMANUSCRIPT", cellEndtIsManuscript)
    endt_sql_complete = Strings.Replace(endt_sql_complete, "PRODUCTVERSIONNAME", cellProductVersion)
    endt_sql_complete = Strings.Replace(endt_sql_complete, "DEFAULTADDITIONALINFO", cellDefaultAddlInfo)
  
    Debug.Print (endt_sql_complete)
    Print #1, endt_sql_complete
    

    
    x = x + 1
Loop

'Subjectivities
tabname = "Subjectivities"
Sheets(tabname).Select
totalrows = Sheets(tabname).UsedRange.Rows.Count
x = 2
Do While x <= totalrows

    cellProductVersion = Trim(Range("D" & x).Value)
    cellSubjSeq = Trim(Range("F" & x).Value)
    cellSubjDesc = Trim(Range("G" & x).Value)
    cellSubjDesc = Strings.Replace(cellSubjDesc, "'", "\'")
    cellSubjCondition = Strings.Replace(cellSubjCondition, "'", "\'")
    cellSubjDueBeforeBinding = Trim(Range("H" & x).Value)
    cellSubjCondition = Trim(Range("I" & x).Value)
    cellSubjIsDefault = Trim(Range("J" & x).Value)
    
    'endt forms
    subjt_sql = "INSERT INTO `subjectivity` (`description`, `conditions`, `sequence`, `product_version_id`, `is_due_before_binding`, `is_default`, `is_active`) VALUES ('DESCRIPTION', 'CONDITION', SEQUENCE, (select id from product_version where name ='PRODUCTVERSIONNAME'), DUEBEFOREBINDING, ISDEFAULT, 1);"
    subjt_sql_complete = subjt_sql
    subjt_sql_complete = Strings.Replace(subjt_sql_complete, "DESCRIPTION", cellSubjDesc)
    subjt_sql_complete = Strings.Replace(subjt_sql_complete, "CONDITION", cellSubjCondition)
    subjt_sql_complete = Strings.Replace(subjt_sql_complete, "SEQUENCE", cellSubjSeq)
    subjt_sql_complete = Strings.Replace(subjt_sql_complete, "ISDEFAULT", cellSubjIsDefault)
    subjt_sql_complete = Strings.Replace(subjt_sql_complete, "DUEBEFOREBINDING", cellSubjDueBeforeBinding)
    subjt_sql_complete = Strings.Replace(subjt_sql_complete, "PRODUCTVERSIONNAME", cellProductVersion)
    
    Debug.Print (subjt_sql_complete)
    Print #1, subjt_sql_complete
    

    
    x = x + 1
Loop

Print #1, "commit;"

Close #1

MsgBox (CurDir() & "\product_configuration.sql created.")
End Sub
