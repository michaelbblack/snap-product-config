SET autocommit = 0;
start transaction;
INSERT INTO `endorsement` (`conditions`, `sequence`, `is_default`, `is_active`, `is_manuscript`, `product_version_id`, `form_id`) VALUES ('', 45, 0, 1, 0, (select id from product_version where name ='NetGuardPlus_20180401'), (select id from form where number ='E1856NIO-0718'));
INSERT INTO `form` (`number`, `url`, `type`, `name`) VALUES ('E1856NIO-0718', 'https://www.nasinsurance.com/var/documents/E1856NIO-0718.pdf', 'endorsement' , 'Bricking Loss Sublimit with Betterment Coverage');
INSERT INTO `endorsement` (`conditions`, `sequence`, `is_default`, `is_active`, `is_manuscript`, `product_version_id`, `form_id`) VALUES ('', 45, 0, 1, 0, (select id from product_version where name ='NetGuardPlus_PPI_20180401'), (select id from form where number ='E1856NIO-0718'));
INSERT INTO `form` (`number`, `url`, `type`, `name`) VALUES ('E1856NIO-0718', 'https://www.nasinsurance.com/var/documents/E1856NIO-0718.pdf', 'endorsement' , 'Bricking Loss Sublimit with Betterment Coverage');
commit;
